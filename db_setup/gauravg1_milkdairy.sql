-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 26, 2018 at 10:50 AM
-- Server version: 5.6.39
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gauravg1_milkdairy`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer_details`
--

CREATE TABLE `customer_details` (
  `customer_id` int(11) NOT NULL,
  `customer_name` text,
  `customer_email` text,
  `customer_contact` text,
  `customer_profile_picture` text,
  `customer_address` text,
  `pending_amount` double DEFAULT '0',
  `account_status` int(11) NOT NULL DEFAULT '1',
  `created_at` text,
  `updated_at` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_details`
--

INSERT INTO `customer_details` (`customer_id`, `customer_name`, `customer_email`, `customer_contact`, `customer_profile_picture`, `customer_address`, `pending_amount`, `account_status`, `created_at`, `updated_at`) VALUES
(1, 'Vijay Mehta.', 'vijay@gmail.com', '9898989889', '1.jpg', 'xyz,pqr 12345.', 2.13, 1, '2018-04-26 12:39:24', '2018-05-16 09:36:12'),
(2, 'test', 'test@gmail.com', '8987878676', 'noimage.jpg', 'pune', 0, 1, '2018-07-09 11:23:56', '2018-07-09 11:23:56');

-- --------------------------------------------------------

--
-- Table structure for table `fats_and_rates`
--

CREATE TABLE `fats_and_rates` (
  `id` int(11) NOT NULL,
  `fats_value` double DEFAULT NULL,
  `rates_per_100_ml` double DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` text,
  `updated_at` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fats_and_rates`
--

INSERT INTO `fats_and_rates` (`id`, `fats_value`, `rates_per_100_ml`, `status`, `created_at`, `updated_at`) VALUES
(1, 3.5, 2.13, 1, NULL, NULL),
(2, 3.6, 2.15, 1, NULL, '2018-04-26 11:32:05'),
(3, 3.7, 2.17, 1, NULL, NULL),
(4, 3.8, 2.19, 1, NULL, NULL),
(5, 3.9, 2.21, 1, NULL, NULL),
(6, 4, 2.23, 1, NULL, NULL),
(7, 4.1, 2.24, 1, NULL, NULL),
(8, 4.2, 2.26, 1, NULL, NULL),
(9, 4.3, 2.28, 1, NULL, NULL),
(10, 4.4, 2.3, 1, NULL, NULL),
(11, 4.5, 2.32, 1, NULL, NULL),
(12, 4.6, 2.34, 1, NULL, NULL),
(13, 4.7, 2.36, 1, NULL, NULL),
(14, 4.8, 2.38, 1, NULL, NULL),
(15, 4.9, 2.4, 1, NULL, NULL),
(16, 5, 2.42, 1, NULL, NULL),
(17, 5.1, 2.43, 1, NULL, NULL),
(18, 5.2, 2.45, 1, NULL, NULL),
(19, 5.3, 2.47, 1, NULL, NULL),
(20, 5.4, 2.49, 1, NULL, NULL),
(21, 5.5, 2.58, 1, NULL, NULL),
(22, 5.6, 2.63, 1, NULL, NULL),
(23, 5.7, 2.67, 1, NULL, NULL),
(24, 5.8, 2.72, 1, NULL, NULL),
(25, 5.9, 2.77, 1, NULL, NULL),
(26, 6, 2.93, 1, NULL, NULL),
(27, 6.1, 2.98, 1, NULL, NULL),
(28, 6.2, 3.02, 1, NULL, NULL),
(29, 6.3, 3.07, 1, NULL, NULL),
(30, 6.4, 3.12, 1, NULL, NULL),
(31, 6.5, 3.17, 1, NULL, NULL),
(32, 6.6, 3.21, 1, NULL, NULL),
(33, 6.7, 3.26, 1, NULL, NULL),
(34, 6.8, 3.31, 1, NULL, NULL),
(35, 6.9, 3.35, 1, NULL, NULL),
(36, 7, 3.4, 1, NULL, NULL),
(37, 7.1, 3.45, 1, NULL, NULL),
(38, 7.2, 3.49, 1, NULL, NULL),
(39, 7.3, 3.54, 1, NULL, NULL),
(40, 7.4, 3.59, 1, NULL, NULL),
(41, 7.5, 3.64, 1, NULL, NULL),
(42, 7.6, 3.68, 1, NULL, NULL),
(43, 7.7, 3.73, 1, NULL, NULL),
(44, 7.8, 3.78, 1, NULL, NULL),
(45, 7.9, 3.82, 1, NULL, NULL),
(46, 8, 3.87, 1, NULL, NULL),
(47, 8.1, 3.92, 1, NULL, NULL),
(48, 8.2, 3.96, 1, NULL, NULL),
(49, 8.3, 4.01, 1, NULL, NULL),
(50, 8.4, 4.06, 1, NULL, NULL),
(51, 8.5, 4.11, 1, NULL, NULL),
(52, 8.6, 4.15, 1, NULL, NULL),
(53, 8.7, 4.2, 1, NULL, NULL),
(54, 8.8, 4.25, 1, NULL, NULL),
(55, 8.9, 4.29, 1, NULL, NULL),
(56, 9, 4.34, 1, NULL, NULL),
(57, 9.1, 4.39, 1, NULL, NULL),
(58, 9.2, 4.43, 1, NULL, NULL),
(59, 9.3, 4.48, 1, NULL, NULL),
(60, 9.4, 4.53, 1, NULL, NULL),
(61, 9.5, 4.58, 1, NULL, NULL),
(62, 9.6, 4.62, 1, NULL, NULL),
(63, 9.7, 4.67, 1, NULL, NULL),
(64, 9.8, 4.72, 1, NULL, NULL),
(65, 9.9, 4.76, 1, NULL, NULL),
(66, 10, 4.81, 1, NULL, NULL),
(69, 10.12, 4.782, 0, '2018-04-26 12:42:52', '2018-04-26 12:43:30');

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE `product_details` (
  `product_id` int(11) NOT NULL,
  `product_name` text,
  `product_price` double DEFAULT NULL,
  `product_description` text,
  `product_status` int(11) NOT NULL DEFAULT '1',
  `created_at` text,
  `updated_at` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_details`
--

INSERT INTO `product_details` (`product_id`, `product_name`, `product_price`, `product_description`, `product_status`, `created_at`, `updated_at`) VALUES
(1, 'Cow Milk', NULL, 'abcd,xyz', 1, '2018-04-26 12:41:14', '2018-04-26 12:41:58'),
(2, 'Buffalo Milk', NULL, 'abcd', 1, '2018-05-16 08:37:25', '2018-05-16 08:37:25');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_master`
--

CREATE TABLE `purchase_order_master` (
  `order_id` int(11) NOT NULL,
  `party_id` int(11) DEFAULT NULL,
  `order_time` text,
  `order_date` date DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `round_off_amount` double DEFAULT NULL,
  `paid_amount` double DEFAULT NULL,
  `created_at` text,
  `updated_at` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order_master`
--

INSERT INTO `purchase_order_master` (`order_id`, `party_id`, `order_time`, `order_date`, `total_amount`, `round_off_amount`, `paid_amount`, `created_at`, `updated_at`) VALUES
(4, 7, 'morning', '2018-05-16', 50, 50, 40, '2018-05-16 08:11:50', '2018-05-16 08:11:50'),
(5, 7, 'morning', '2018-05-16', 50, 50, 45, '2018-05-16 08:23:48', '2018-05-16 08:23:48'),
(6, 7, 'evening', '2018-05-15', 150, 150, 100, '2018-05-16 08:35:35', '2018-05-16 08:35:35'),
(7, 7, 'evening', '2018-05-16', 100, 100, 100, '2018-05-16 08:38:02', '2018-05-16 08:38:02'),
(8, 7, 'evening', '2018-05-17', 5, 5, 5, '2018-05-16 09:34:44', '2018-05-16 09:34:44');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_transactions`
--

CREATE TABLE `purchase_order_transactions` (
  `transaction_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `total_liters` double DEFAULT NULL,
  `total_fats` double DEFAULT NULL,
  `unit_price` double DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `created_at` text,
  `updated_at` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order_transactions`
--

INSERT INTO `purchase_order_transactions` (`transaction_id`, `order_id`, `product_id`, `total_liters`, `total_fats`, `unit_price`, `total_price`, `created_at`, `updated_at`) VALUES
(4, 4, 1, 1000, 2, 5, 50, '2018-05-16 08:11:50', '2018-05-16 08:11:50'),
(5, 5, 1, 1000, 20, 5, 50, '2018-05-16 08:23:48', '2018-05-16 08:23:48'),
(6, 6, 1, 3000, 12, 5, 150, '2018-05-16 08:35:35', '2018-05-16 08:35:35'),
(7, 7, 1, 1000, 2, 5, 50, '2018-05-16 08:38:02', '2018-05-16 08:38:02'),
(8, 7, 2, 1000, 2, 5, 50, '2018-05-16 08:38:02', '2018-05-16 08:38:02'),
(9, 8, 1, 100, 3, 5, 5, '2018-05-16 09:34:44', '2018-05-16 09:34:44');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_party_details`
--

CREATE TABLE `purchase_party_details` (
  `id` int(11) NOT NULL,
  `name` text,
  `email` text,
  `contact` text,
  `profile_pic` text,
  `address` text,
  `pending` double NOT NULL DEFAULT '0',
  `account_status` int(11) NOT NULL DEFAULT '1',
  `created_at` text,
  `updated_at` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_party_details`
--

INSERT INTO `purchase_party_details` (`id`, `name`, `email`, `contact`, `profile_pic`, `address`, `pending`, `account_status`, `created_at`, `updated_at`) VALUES
(7, 'Party 1', 'part@gmail.com', '8954857845', 'noimage.jpg', 'abcd', 65, 1, '2018-05-16 08:11:12', '2018-05-16 08:35:35');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_product_rate`
--

CREATE TABLE `purchase_product_rate` (
  `id` int(11) NOT NULL,
  `rate` double NOT NULL DEFAULT '0',
  `created_at` text,
  `updated_at` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_product_rate`
--

INSERT INTO `purchase_product_rate` (`id`, `rate`, `created_at`, `updated_at`) VALUES
(1, 5, NULL, '2018-05-16 05:34:48');

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_master`
--

CREATE TABLE `sales_order_master` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `order_time` text,
  `order_date` date DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `round_off_amount` double DEFAULT NULL,
  `paid_amount` double DEFAULT NULL,
  `created_at` text,
  `updated_at` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_order_master`
--

INSERT INTO `sales_order_master` (`order_id`, `customer_id`, `order_time`, `order_date`, `total_amount`, `round_off_amount`, `paid_amount`, `created_at`, `updated_at`) VALUES
(1, 1, 'morning', '2018-04-26', 4.94, 4.94, 4.94, '2018-04-26 12:45:27', '2018-04-26 12:45:27'),
(2, 1, 'morning', '2018-05-06', 24.7, 24.7, 24.7, '2018-05-06 03:45:50', '2018-05-06 03:45:50'),
(3, 1, 'morning', '2018-05-16', 4.62, 4.62, 4.62, '2018-05-16 09:18:38', '2018-05-16 09:18:38'),
(4, 1, 'morning', '2018-05-16', 2.13, 2.13, 0, '2018-05-16 09:36:12', '2018-05-16 09:36:12');

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_transactions`
--

CREATE TABLE `sales_order_transactions` (
  `transaction_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `total_liters` double DEFAULT NULL,
  `total_fats` double DEFAULT NULL,
  `unit_price` double DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `created_at` text,
  `updated_at` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_order_transactions`
--

INSERT INTO `sales_order_transactions` (`transaction_id`, `order_id`, `product_id`, `total_liters`, `total_fats`, `unit_price`, `total_price`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 200, 5.3, 2.47, 4.94, '2018-04-26 12:45:28', '2018-04-26 12:45:28'),
(2, 2, 1, 1000, 5.3, 2.47, 24.7, '2018-05-06 03:45:51', '2018-05-06 03:45:51'),
(3, 3, 1, 100, 3.5, 2.13, 2.13, '2018-05-16 09:18:38', '2018-05-16 09:18:38'),
(4, 3, 2, 100, 5.4, 2.49, 2.49, '2018-05-16 09:18:38', '2018-05-16 09:18:38'),
(5, 4, 1, 100, 3.5, 2.13, 2.13, '2018-05-16 09:36:12', '2018-05-16 09:36:12');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `company_name` text,
  `company_contact` text,
  `company_address` text,
  `created_at` text,
  `updated_at` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `company_name`, `company_contact`, `company_address`, `created_at`, `updated_at`) VALUES
(1, 'Gokul Dairy', '9845784758', 'abc, xyz, 892334', NULL, '2018-05-16 09:35:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` text,
  `email` text,
  `password` text,
  `remember_token` text,
  `created_at` text,
  `updated_at` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dairy Application', 'admin@gmail.com', '$2y$10$9uGOMQhnDTjwC75pGweGMOECmb8Lfs6VNWehyVAjHF8y6DLhu69im', 'G0fKKv3o2P8b2B5hisvmw7bhO3VgWbxZIZ4lhOr3w46tL7a6JwetdHdujvUU', '2018-04-22 18:49:16', '2018-04-22 18:49:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer_details`
--
ALTER TABLE `customer_details`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `fats_and_rates`
--
ALTER TABLE `fats_and_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_details`
--
ALTER TABLE `product_details`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `purchase_order_master`
--
ALTER TABLE `purchase_order_master`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `purchase_order_transactions`
--
ALTER TABLE `purchase_order_transactions`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `purchase_party_details`
--
ALTER TABLE `purchase_party_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_product_rate`
--
ALTER TABLE `purchase_product_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order_master`
--
ALTER TABLE `sales_order_master`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `sales_order_transactions`
--
ALTER TABLE `sales_order_transactions`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer_details`
--
ALTER TABLE `customer_details`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fats_and_rates`
--
ALTER TABLE `fats_and_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `product_details`
--
ALTER TABLE `product_details`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `purchase_order_master`
--
ALTER TABLE `purchase_order_master`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `purchase_order_transactions`
--
ALTER TABLE `purchase_order_transactions`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `purchase_party_details`
--
ALTER TABLE `purchase_party_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `purchase_product_rate`
--
ALTER TABLE `purchase_product_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sales_order_master`
--
ALTER TABLE `sales_order_master`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sales_order_transactions`
--
ALTER TABLE `sales_order_transactions`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
