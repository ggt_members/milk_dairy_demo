<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fats_rate_model extends Model
{
     protected $table="fats_and_rates";
    protected $primaryKey="id";  
    protected $timestamp=true;	
}
