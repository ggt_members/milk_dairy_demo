<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products_model extends Model
{
    protected $table="product_details";
    protected $primaryKey="product_id";  
    protected $timestamp=true;	
}
