<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sales_order_master extends Model
{
    protected $table="sales_order_master";
    protected $primaryKey="order_id";  
    protected $timestamp=true;	
}
