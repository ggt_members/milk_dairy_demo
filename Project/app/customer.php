<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    protected $table="customer_details";
    protected $primaryKey="customer_id";  
    protected $timestamp=true;	
}
