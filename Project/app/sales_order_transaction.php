<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sales_order_transaction extends Model
{
      protected $table="sales_order_transactions";
    protected $primaryKey="transaction_id";  
    protected $timestamp=true;	
}
