<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class setting_model extends Model
{
    protected $table="setting";
    protected $primaryKey="id";  
    protected $timestamp=true;	
}
