<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\fats_rate_model;
use Illuminate\Support\Facades\DB; 

class fats_rate_controller extends Controller
{
    
		function insertRecord(){
			
			
							try{
						//TODO-Fetch Input values 
						$data = Input::all();
								
						//print_r($data);
						
								$fats_rate_model=new fats_rate_model;
								
								$fats_rate_model->fats_value=$data['fat_value'];
								$fats_rate_model->rates_per_100_ml=$data['fat_price'];
								
								//TODO - Insert Data Into table.
								$result=$fats_rate_model->save();
									
								
								if($result==1){
									
									return redirect("new_fats?status=1");  //Success
									
								}else{
									
									
									return redirect("new_fats?status=0");  //Fail
								}								
									
						
		
				}catch(\Exception $e){
		       
					//echo $e->getMessage();

					return redirect("new_fats?status=0");  //Fail
					

				} //End of try catch block
			
		}//End of fucntion


		public function get_all_fat_rates(){
		
		
				//TODO-Get all records from table		
				$Records=fats_rate_model::select('id','fats_value','rates_per_100_ml')->where('status',1)->get();
	
				$response=array();
		
				//TODO - Iterate through every record and add it into array
				foreach($Records as $Record){
					
						//echo "<br>Id=".$Record->product_id." , Name=".$Record->product_name;

						$response[]=array(

							"id" => $Record->id,
							"fat_value" => $Record->fats_value,
							"rate_per_100_ml" => $Record->rates_per_100_ml
							

						);
					
				}//End of foreach loop
	
				//TODO - Convert array into JSON Format	
				return json_encode($response);
		
	
	}//End of slect all function



	function allFatRates(){
		$fats_rate_model=new fats_rate_model;
		$fats_rate_data =DB::table('fats_and_rates')->where('status',1)->get();
		$i = 1;
		$view = array(
		              'data'=>$fats_rate_data
		            );
		return view('update_fat_rate_list', compact('fats_rate_data','i'));
	}


	function updateRecord(){
			
			
							try{
						//TODO-Fetch Input values 
						$data = Input::all();
								
						//print_r($data);
						
								$fats_rate_model=fats_rate_model::find($data['id']);
								
								$fats_rate_model->fats_value=$data['fat_value'];
								$fats_rate_model->rates_per_100_ml=$data['fat_price'];
								
								//TODO - Insert Data Into table.
								$result=$fats_rate_model->save();
									
								
								if($result==1){
									
									return redirect("all_fat_list?status=1");  //Success
									
								}else{
									
									
									return redirect("all_fat_list?status=0");  //Fail
								}								
									
						
		
				}catch(\Exception $e){
		       
					//echo $e->getMessage();

					return redirect("all_fat_list?status=0");  //Fail
					

				} //End of try catch block
			
		}//End of fucntion


		
		public function disableRecord(){
		
		
		try{
		
				//TODO-Fetch Input values 
				$data = Input::all();
			
				//TODO-Make Object Of Product Master Model
				$fats_rate_model=fats_rate_model::find($data['id']);

				
				$fats_rate_model->status=0;

				//TODO - Insert Data Into table.
				$result=$fats_rate_model->save();	
				
				if($result){
				
					//return view('all_customer_list')->with("status",1);
					return redirect("all_fat_list?delete_status=1");
					
				}else{
					
					//return view('all_customer_list')->with("status",0);
					return redirect("all_fat_list?delete_status=0");
				}
			
		
		  }catch(\Exception $e){
		       
				    //return view('all_customer_list')->with("status",0);
					return redirect("all_fat_list?delete_status=0");

		  } //End of try catch block	

		
		
		
	}//End of function
	

		



		
	
}//End of class
