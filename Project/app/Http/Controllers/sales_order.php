<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\sales_order_master;
use App\sales_order_transaction;
use App\customer;
use App\setting_model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class sales_order extends Controller
{
    
	
		public function insert(){
			
				$sales_order_id=-1;
				//TODO - Exceptio Handling 
						try{
								//TODO-Fetch Input values 
								$input_data = Input::all();
							
								//TODO-Decode Json Data
							    $data=json_decode($input_data['jsonData'],true);
								//print_r($data);
								
								
								$sales_order_master=new sales_order_master;
								
								$sales_order_master->customer_id=$data[0]['customer_id'];
								$sales_order_master->order_time=$data[0]['time'];
								$sales_order_master->order_date=$data[0]['date'];
								$sales_order_master->total_amount=$data[0]['total_amount'];
								$sales_order_master->round_off_amount=$data[0]['round_off'];
								$sales_order_master->paid_amount=$data[0]['paid_amount'];
								
								$Result=$sales_order_master->save();
								
								if($Result==1){
									
									
											$sales_order_id=$sales_order_master->order_id;
									
											//TODO - MAke ARRAY OF DATA AND INSERT IT INTO Sales Order Master
											$Records=array();	
											$time=date("Y-m-d h:i:s");
											
											
											//TODO - Make New Json Array	
											for($i=0;$i<sizeof($data[1]);$i++){

												$Records[]=array(
																	'order_id'=>$sales_order_id,
																	'product_id'=>$data[1][$i]['product_id'],
																	'total_liters'=>$data[1][$i]['liters'],
																	'total_fats'=>$data[1][$i]['fats'],
																	'unit_price'=>$data[1][$i]['unit_price'],															
																	'total_price'=>$data[1][$i]['total_price'],
																	'created_at'=>$time,
																	'updated_at'=>$time
																

															);

											
						


										}//End of for loop

											
											//Insert Record Into PO Master
										$Result=sales_order_transaction::insert($Records);
										
											if($Result==1){

												//Update Product Quantity Into Product Master
												$update_customer_pending_amt = customer::find($data[0]['customer_id']);
												$update_customer_pending_amt->pending_amount+= ($data[0]['round_off'] - $data[0]['paid_amount'] );
												$update_customer_pending_amt->save();	
											
												return $sales_order_id;
												
											}else{

												return -1;
											}											
									
									
								}else{
									return -1;
								}
								
								
						   }catch(\Exception $e){
							
							//echo $e->getMessage();

							return -1;   //Exception Occcur fail to insert

						  } //End of try catch block	
			
			
			
		}//End of function


		
		public function invoiceData(){
			
				
			try{
			
				//TODO-Fetch Input values 
				$input_data = Input::all();
				$id=$input_data['order_id'];
				
		
		
		$company_details = DB::table('setting')
                      ->select( 'setting.company_name','setting.company_contact','setting.company_address')
					->where('setting.id',1)
                     ->get();
		
		

		$cust_details = DB::table('sales_order_master')
                     ->join('customer_details', 'sales_order_master.customer_id', '=', 'customer_details.customer_id')
					  ->select( 'customer_details.customer_name','customer_details.customer_contact','sales_order_master.order_id','sales_order_master.order_time',
					  'sales_order_master.order_date','sales_order_master.total_amount','sales_order_master.round_off_amount','sales_order_master.paid_amount')
					->where('sales_order_master.order_id',$id)
                     ->get();
			
				
				
				
				//print_r($cust_details);
				
				//echo "<br><br>";
				
				
				
				
			$order_details = DB::table('sales_order_master')
                     ->join('sales_order_transactions', 'sales_order_master.order_id', '=', 'sales_order_transactions.order_id')	
					 ->join('product_details', 'product_details.product_id', '=', 'sales_order_transactions.product_id')	
					 ->select('product_details.product_name','sales_order_transactions.total_liters','sales_order_transactions.total_fats','sales_order_transactions.unit_price',
							  'sales_order_transactions.total_price'
							 )
				
					->where('sales_order_master.order_id',$id)	
                     ->get();
			
			
				//print_r($order_details);
				
				//die();
				

				return view('invoice',['company_details'=>$company_details,'cust_details'=>$cust_details,'order_details'=>$order_details]);
			
			 }catch(\Exception $e){
							
						echo $e->getMessage();
			
						$company_details =array();
						$cust_details=array();
						$order_details=array();	
			//	return view('invoice',['cust_details'=>$cust_details,'order_details'=>$order_details]);
			return view('invoice',['company_details'=>$company_details,'cust_details'=>$cust_details,'order_details'=>$order_details]);
			
			} //End of try catch block
			
			
			
		}//End of function
		
		
		
		
		public function displaySalesOrderData(){
			
			
				$cust_details = DB::table('sales_order_master')
                     ->join('customer_details', 'sales_order_master.customer_id', '=', 'customer_details.customer_id')
					  ->select( 'customer_details.customer_name','customer_details.customer_contact','sales_order_master.order_id','sales_order_master.order_time',
					  'sales_order_master.order_date','sales_order_master.total_amount','sales_order_master.round_off_amount','sales_order_master.paid_amount')
					->orderBy('sales_order_master.created_at','desc')	
					 ->get();
			
				
				//print_r($cust_details);
				
				return view('sales_order_records',['data'=>$cust_details]);
			
		}//End of funcition 
		
		
		
			function generateReport(){

			$input_data = Input::all();

			if(Input::has('from_date')  && Input::has('to_date')){

				$from_date=$input_data['from_date'];
				$to_date=$input_data['to_date'];

				$record = DB::table('sales_order_master')
                     ->join('sales_order_transactions', 'sales_order_master.order_id', '=', 'sales_order_transactions.order_id')	
					 ->join('product_details', 'product_details.product_id', '=', 'sales_order_transactions.product_id')
					 ->join('customer_details', 'customer_details.customer_id', '=', 'sales_order_master.customer_id')	
					 ->select('customer_details.customer_name',
					 			'sales_order_master.order_date',
					 			'sales_order_master.order_time',
							 	'product_details.product_name',
							 	'sales_order_transactions.total_liters',
							 	'sales_order_transactions.total_fats',
							 	'sales_order_transactions.unit_price',
								'sales_order_transactions.total_price'
							)->whereBetween('sales_order_master.order_date', [$from_date, $to_date])
								->orderBy('sales_order_master.created_at','desc')	
				
					->get();	

				return view('Report',['data'=>$record]);

			}else{

				$record = DB::table('sales_order_master')
                     ->join('sales_order_transactions', 'sales_order_master.order_id', '=', 'sales_order_transactions.order_id')	
					 ->join('product_details', 'product_details.product_id', '=', 'sales_order_transactions.product_id')
					 ->join('customer_details', 'customer_details.customer_id', '=', 'sales_order_master.customer_id')	
					 ->select('customer_details.customer_name',
					 			'sales_order_master.order_date',
					 			'sales_order_master.order_time',
							 	'product_details.product_name',
							 	'sales_order_transactions.total_liters',
							 	'sales_order_transactions.total_fats',
							 	'sales_order_transactions.unit_price',
								'sales_order_transactions.total_price'
							)	->orderBy('sales_order_master.created_at','desc')	
				->get();	

				return view('Report',['data'=>$record]);

			}

					 
                     
		}


		function generateBalance(){

			$input_data = Input::all();
			

			if(Input::has('from_date')){

				$from_date=$input_data['from_date'];

				$closing = DB::table("sales_order_master")
						->where('sales_order_master.order_date', '<=', $from_date)
						->sum('sales_order_master.round_off_amount');

				$opening = DB::table("sales_order_master")
							->where('sales_order_master.order_date', '<', $from_date)
							->sum('sales_order_master.round_off_amount');
							
							//$record = strval($recordcount);
				return view('opening_and_closing_balance',['closing'=>$closing, 'opening'=>$opening]);

			}else{

				$closing = DB::table("sales_order_master")
						->sum('sales_order_master.round_off_amount');

				$opening = DB::table("sales_order_master")
							->where('sales_order_master.order_date', '<', Carbon::today())
							->sum('sales_order_master.round_off_amount');
							
							//$record = strval($recordcount);
				return view('opening_and_closing_balance',['closing'=>$closing, 'opening'=>$opening]);
			
			}

						
		}
		
		
}//End of class
