<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\setting_model;

class setting_controller extends Controller
{
   
	public function getSetttingData(){
		
			//TODO-Get all records from table		
				$Records=setting_model::select('id','company_name','company_contact','company_address')->get();
	
				
				$response=array();
		
				//TODO - Iterate through every record and add it into array
				foreach($Records as $Record){
					
						//echo "<br>Id=".$Record->product_id." , Name=".$Record->product_name;

						$response["company_id"]=$Record->id;
						$response["company_name"]=$Record->company_name;
						$response["company_contact"]=$Record->company_contact;
						$response["company_address"]=$Record->company_address;

					
					
				}//End of foreach loop
	
				//TODO - Convert array into JSON Format	
				return view('Setting')->with("data",$response);
		
	}//End of function
   
   
	
	public function updateSettingData(){
		
			try{
				
				$input_data=input::all();				
				$setting_model=setting_model::find(1);				
				$setting_model->company_name=$input_data['company_name'];
				$setting_model->company_contact=$input_data['company_contact'];
				$setting_model->company_address=$input_data['company_address'];				
				
				$result=$setting_model->save();
			 
				if($result){
				
					//return view('all_customer_list')->with("status",1);
					return redirect("Setting?status=1");
					
				}else{
					
					//return view('all_customer_list')->with("status",0);
					return redirect("Setting?status=0");
				}
			 
			
			 }catch(\Exception $e){
		       
				//echo $e->getMessage();

				return redirect("Setting?status=0");
				

			} //End of try catch block

			
		
	}//End of function
   
   
}//End of class
