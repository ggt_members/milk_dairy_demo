<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\customer as customer_model;


class customer extends Controller
{
    
	 function insert_new_customer(){
		//This function is used to add new customer into database.
		
			//TODO - Exception Handling 
		try{
						//TODO-Fetch Input values 
								$data = Input::all();
								
								//print_r($data);
						
						/**/
						//$cusid = $custid->customer_id + 1; 
						
									
						$customer_model=new customer_model;
						
						$customer_model->customer_name=$data['customer_name'];
						$customer_model->customer_email=$data['customer_email'];
						$customer_model->customer_contact=$data['customer_contact'];
						$customer_model->customer_address=$data['customer_address'];
						
						
						//TODO - Insert Data Into table.
						$result=$customer_model->save();
							
						$custid = customer_model::orderby('created_at', 'desc')->first();


						$destinationPath = 'customer_profile_pic';

						//Handle file upload 
				        if(Input::has('customer_profile_pic')){

				        	if(Input::file('customer_profile_pic')->getSize()<=2000000){

					            $filenameWithExt = Input::file('customer_profile_pic')->getClientOriginalName();
					            //Get file name
					            $fileName = pathinfo($filenameWithExt, PATHINFO_FILENAME);
					            //Get Extention
					            $extension = Input::file('customer_profile_pic')->getClientOriginalExtension();
					            //File to store
					            $fileNameToStore = $custid->customer_id.'.'.$extension;
					            //Upload
					            $path = Input::file('customer_profile_pic')->move($destinationPath, $fileNameToStore); 
				        	}else{
				        		return redirect("new_customer?status=2");
				        	}
				        	
				        }else{
				            $fileNameToStore = 'noimage.jpg';
				        }

				        //TODO-Make Object Of Product Master Model
						$customer=customer_model::find($custid->customer_id);

						//TODO-Add Data inTo created Object
						$customer->customer_profile_picture = $fileNameToStore;

						//TODO - Insert Data Into table.
						$result=$customer->save();	

						//result=$customer_model->save();

						if($result==1){
							
							return redirect("new_customer?status=1");  //Success
							
						}else{
							
							
							return redirect("new_customer?status=0");  //Fail
						}								
									
								//return view('update_customer_details')->with("data",$data);
								
			 
			}catch(\Exception $e){
		       
			//echo $e->getMessage();

		  	return redirect("new_customer?status=0");  //Fail
			

			} //End of try catch block


		  
	}//End of function
	
	
	
	
	 function show_update_page(){
		
		
		//TODO-Fetch Input values 
				$data = Input::all();
				
				
				return view('update_customer_details')->with("data",$data);
				
		
	}//End of function
	
	
	
	function get_all_customers(){
		
		
		
				//TODO-Get all records from table		
				$Records=customer_model::select('customer_id','customer_name')->where('account_status',1)->get();
	
				$response=array();
		
				//TODO - Iterate through every record and add it into array
				foreach($Records as $Record){
					
						//echo "<br>Id=".$Record->product_id." , Name=".$Record->product_name;

						$response[]=array(

							"customer_id" => $Record->customer_id,
							"customer_name" => $Record->customer_name

						);
					
				}//End of foreach loop
	
				//TODO - Convert array into JSON Format	
				return json_encode($response);
	
	
	
	} //End of get_all_customer function
	
	
	public function getSelectedCustomerDetails(){
		
			//TODO - Exception Handling 
		try{
					
				//TODO-Fetch Input values 
				$data = Input::all();
		
				$id=$data['id'];
				
				//TODO-Get all records from table		
				$Records=customer_model::select('customer_contact')->where('customer_id',$id)->get();
	
				
		
				$response=array();
		
				//TODO - Iterate through every record and add it into array
				foreach($Records as $Record){
					
						//echo "<br>Id=".$Record->product_id." , Name=".$Record->product_name;

						$response=array(

							"customer_contact" => $Record->customer_contact,
							//"customer_pending_amount" => $Record->customer_pending_amount,
							//"customer_address" => $Record->customer_address,


						);
					
				}//End of foreach loop
	
				//TODO - Convert array into JSON Format	
				return json_encode($response);
			
			}catch(\Exception $e){
		       
			//echo $e->getMessage();

		  	return "";  //Fail
			

			} //End of try catch block

		
		
	
	}//End of slect all function
	
	
	
	
	
	function getCustomerData(){
		$customer_model=new customer_model;
		$customerData = DB::table('customer_details')->where('account_status',1)->orderBy('customer_id', 'asc')->get();
		$i = 1;
		$view = array(
		              'name'=>$customerData
		            );
		return view('update_customer_list', compact('customerData','i'));
	}//End of function
	
	
	
	function selectSpecificFromCustomerTable($id=3){
		
		
			//TODO-Fetch Input values 
				$data = Input::all();
		
				$id=$data['id'];
		
			//TODO-Get all records from table		
				$Records=customer_model::select()->where('customer_id',$id)->get();
	
				//print_r($Records[0]);
		
				//echo $Records[0]->customer_name;
		
				return view('update_customer_details')->with("Record",$Records[0]);
				
		
	}

	
	
	
	function updateRecord(){
		
		try{
		
				//TODO-Fetch Input values 
				$data = Input::all();
			
				//TODO-Make Object Of Product Master Model
				$customer=customer_model::find($data['cust_id']);

				//TODO-Add Data inTo created Object
				$customer->customer_name=$data['customer_name'];
				$customer->customer_address=$data['customer_address'];
				$customer->customer_contact=$data['customer_contact_no'];
				$customer->customer_email=$data['customer_email'];


				$destinationPath = 'customer_profile_pic';

						//Handle file upload 
				        if(Input::has('customer_profile_pic')){

				        	if(Input::file('customer_profile_pic')->getSize()<=2000000){

					            $filenameWithExt = Input::file('customer_profile_pic')->getClientOriginalName();
					            //Get file name
					            $fileName = pathinfo($filenameWithExt, PATHINFO_FILENAME);
					            //Get Extention
					            $extension = Input::file('customer_profile_pic')->getClientOriginalExtension();
					            //File to store
					            $fileNameToStore = $customer->customer_id.'.'.$extension;
					            //Upload
					            $path = Input::file('customer_profile_pic')->move($destinationPath, $fileNameToStore);
					            //database
					            $customer->customer_profile_picture = $fileNameToStore; 
				        	}else{
				        		return redirect("update_customer_list?status=2");
				        	}
				        	
				        }else{
				            $customer->customer_profile_picture = $customer->customer_profile_picture;
				        }






				//TODO - Insert Data Into table.
				$result=$customer->save();	
				
				if($result){
				
					//return view('all_customer_list')->with("status",1);
					return redirect("update_customer_list?status=1");
					
				}else{
					
					//return view('all_customer_list')->with("status",0);
					return redirect("update_customer_list?status=0");
				}
			
		
		  }catch(\Exception $e){
		       
				    //return view('all_customer_list')->with("status",0);
					return redirect("update_customer_list?status=0");

		  } //End of try catch block	

		
		
	}//End of function
	
	
	public function disabledCustomer(){
		
			try{
			
						//TODO-Fetch Input values 
						$data = Input::all();
						
						//TODO-Make Object Of Product Master Model
						$customer=customer_model::find($data['id']);

						//TODO-Add Data inTo created Object
						$customer->account_status=0;
				
						$result=$customer->save();
						
						if($result==1){
							
							return redirect("update_customer_list?delete_status=1");

							
						}else{
							
							return redirect("update_customer_list?delete_status=0");

						}
		

			}catch(\Exception $e){
		       
				    //return view('all_customer_list')->with("status",0);
					return redirect("update_customer_list?delete_status=0");

		   } //End of try catch block	
		
		
	}//End of disabled function
	
	
	
}//End of class
