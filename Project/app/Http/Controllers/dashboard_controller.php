<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\sales_order_master;
use App\sales_order_transaction;
use App\customer;
use Illuminate\Support\Facades\DB;

class dashboard_controller extends Controller
{
    
		public function dashboardData(){
			
				//Ge total Revenue and total paid amount
				$Revenue = DB::table('sales_order_master')->sum('round_off_amount');
				
				//Ge total Revenue and total paid amount
				$paid_amount = DB::table('sales_order_master')->sum('paid_amount');
				
				//Ge total liter and total fats
				$liters = DB::table('sales_order_transactions')->sum('total_liters');
				
				//Ge total liter and total fats
				$fats = DB::table('sales_order_transactions')->sum('total_fats');
				
				
				$start_pg_content=array();
				$start_pg_content['revenue']=$Revenue;
				$start_pg_content['paid_amt']=$paid_amount;
				$start_pg_content['liters']=$liters;
				$start_pg_content['fats']=$fats;
				
				
				
				$recent_orders = DB::table('sales_order_master')
                     ->join('customer_details', 'sales_order_master.customer_id', '=', 'customer_details.customer_id')
					  ->select( 'customer_details.customer_name','sales_order_master.order_time',
					  'sales_order_master.order_date','sales_order_master.total_amount','sales_order_master.round_off_amount','sales_order_master.paid_amount')
					->orderBy('sales_order_master.created_at', 'desc')
					->take(8)
					->get();
				
				
				return view('homepage',['start_pg_content'=>$start_pg_content,'records'=>$recent_orders ]);
				
				
			/*
			echo  $Revenue; echo "<br>";
			echo  $paid_amount;echo "<br>";
			echo $liters;
			echo "<br>";
			echo $fats;
			echo "<br>";
			print_r($recent_orders);
			*/
			
		}//End of function
	
	
	
}//End of class`
