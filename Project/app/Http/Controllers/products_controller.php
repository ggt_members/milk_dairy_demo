<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\products_model;
use Illuminate\Support\Facades\DB; 

class products_controller extends Controller
{
    function insert_new_product(){
		
			try{
						//TODO-Fetch Input values 
						$data = Input::all();
								
						//print_r($data);
						
								$products_model=new products_model;
								
								$products_model->product_name=$data['product_name'];
							//	$products_model->product_price=$data['product_price'];
								$products_model->product_description=$data['product_description'];
								
								//TODO - Insert Data Into table.
								$result=$products_model->save();
									
								
								if($result==1){
									
									return redirect("new_product?status=1");  //Success
									
								}else{
									
									
									return redirect("new_product?status=0");  //Fail
								}								
									
						
		
				}catch(\Exception $e){
		       
					//echo $e->getMessage();

					return redirect("new_product?status=0");  //Fail
					

				} //End of try catch block

	}//End of insert_new_product function
	
	
	
	 function show_update_page(){
		
		
		//TODO-Fetch Input values 
				$data = Input::all();
				
				//print_r($data);
				
				return view('update_product_details')->with("data",$data);
				
		
	}//End of function
	
	
	
	
	public function get_all_products(){
		
		
				//TODO-Get all records from table		
				$Records=products_model::select('product_id','product_name')->where('product_status',1)->get();
	
				$response=array();
		
				//TODO - Iterate through every record and add it into array
				foreach($Records as $Record){
					
						//echo "<br>Id=".$Record->product_id." , Name=".$Record->product_name;

						$response[]=array(

							"product_id" => $Record->product_id,
							"product_name" => $Record->product_name,
							

						);
					
				}//End of foreach loop
	
				//TODO - Convert array into JSON Format	
				return json_encode($response);
		
	
	}//End of slect all function
	
/*	
	public function selectSpecificRecord(){
		
			try{
				
				//TODO-Fetch Input values 
				$data = Input::all();
		
				$id=$data['id'];
				//$id=3;
		
				//TODO-Get all records from table		
				$Records=products_model::select('product_price')->where('product_id',$id)->get();
	
		
		
				$response=array();
		
				//TODO - Iterate through every record and add it into array
				foreach($Records as $Record){
					
						//echo "<br>Id=".$Record->product_id." , Name=".$Record->product_name;

						$response=array(

							
							"product_price" => $Record->product_price,
							
						);
					
				}//End of foreach loop
	
				//TODO - Convert array into JSON Format	
				return json_encode($response);
		
			}catch(\Exception $e){
		       
			//echo $e->getMessage();

		  	return "";  //Fail
			

			} //End of try catch block

		
		
		
	
	}//End of slect specific function
	
	*/

	
	function getProductData(){
		$product_model=new products_model;
		$productData =DB::table('product_details')->where('product_status',1)->orderBy('product_id', 'asc')->get();
		$i = 1;
		$view = array(
		              'name'=>$productData
		            );
		return view('update_product_list', compact('productData','i'));
	}

	
		function selectSpecificFromProductTable($id=3){
		
		
			//TODO-Fetch Input values 
				$data = Input::all();
		
				$id=$data['id'];
		
			//TODO-Get all records from table		
				$Records=products_model::select()->where('product_id',$id)->get();
	
				//print_r($Records[0]);
		
				//echo $Records[0]->customer_name;
		
				return view('update_product_details')->with("Record",$Records[0]);
				
		
	}
	

	function updateRecord(){
		
		try{
		
				//TODO-Fetch Input values 
				$data = Input::all();
			
				//TODO-Make Object Of Product Master Model
				$product=products_model::find($data['product_id']);

				//TODO-Add Data inTo created Object
				$product->product_name=$data['product_name'];
				//$product->product_price=$data['product_price'];
				$product->product_description=$data['product_description'];

				//TODO - Insert Data Into table.
				$result=$product->save();	
				
				if($result){
				
					//return view('all_customer_list')->with("status",1);
					return redirect("update_product_list?status=1");
					
				}else{
					
					//return view('all_customer_list')->with("status",0);
					return redirect("update_product_list?status=0");
				}
			
		
		  }catch(\Exception $e){
		       
				    //return view('all_customer_list')->with("status",0);
					return redirect("update_product_list?status=0");

		  } //End of try catch block	

		
		
	}//End of function
	
	
	public function disableProduct(){
		
		
		try{
		
				//TODO-Fetch Input values 
				$data = Input::all();
			
				//TODO-Make Object Of Product Master Model
				$product=products_model::find($data['id']);

				
				$product->product_status=0;

				//TODO - Insert Data Into table.
				$result=$product->save();	
				
				if($result){
				
					//return view('all_customer_list')->with("status",1);
					return redirect("update_product_list?delete_status=1");
					
				}else{
					
					//return view('all_customer_list')->with("status",0);
					return redirect("update_product_list?delete_status=0");
				}
			
		
		  }catch(\Exception $e){
		       
				    //return view('all_customer_list')->with("status",0);
					return redirect("update_product_list?delete_status=0");

		  } //End of try catch block	

		
		
		
	}//End of function
	
	
	
	
	
	
}//End of class
