<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {

    return view('auth.login');

});

/*
Route::get('/index', function () {

    return view('homepage');

})->middleware('auth');
*/
Route::get('/index','dashboard_controller@dashboardData')->middleware('auth');

//------------------------------------------------------------ CUSTOMER TAB ROUTES -------------------------------------------------
//Display add new customer form
Route::get('/new_customer', function () {

    return view('add_new_customer');
	
})->middleware('auth');

//Add new customer into database
Route::post('/add_new_customer','customer@insert_new_customer')->middleware('auth');


Route::get('/update_customer_list','customer@getCustomerData')->middleware('auth');


Route::get('/update_customer','customer@selectSpecificFromCustomerTable')->middleware('auth');

Route::post('/update_customer_record','customer@updateRecord')->middleware('auth');


//All Customers list
Route::post('/all_customer_records','customer@get_all_customers')->middleware('auth');
//Specific Customers list
Route::post('/specific_customer_record','customer@getSelectedCustomerDetails')->middleware('auth');

//Delete Customers 
Route::get('/delete_customer','customer@disabledCustomer')->middleware('auth');

//---------------------------------------------------------- PRODUCT TAB ROUTES ---------------------------------------------------------

//Route::get('/Update_Product_Details', 'products_controller@show_update_page')->middleware('auth');

//Add new product into database
Route::post('/add_new_product','products_controller@insert_new_product')->middleware('auth');


Route::get('/new_product', function () {

    return view('add_new_product');

})->middleware('auth');


Route::get('/update_product_list','products_controller@getProductData')->middleware('auth');

Route::get('/update_product','products_controller@selectSpecificFromProductTable')->middleware('auth');

Route::post('/update_product_record','products_controller@updateRecord')->middleware('auth');



//All product list
Route::post('/all_product_records','products_controller@get_all_products')->middleware('auth');

//specific product record
Route::post('/specific_product_record','products_controller@selectSpecificRecord')->middleware('auth');


//Delete Customers 
Route::get('/delete_product','products_controller@disableProduct')->middleware('auth');


//-------------------------------------------------------- SALES ORDER  --------------------------------------------------------

Route::post('/insert_sales_order_entry','sales_order@insert')->middleware('auth');

Route::get('/sales_order_history','sales_order@displaySalesOrderData')->middleware('auth');

Route::get('/sales_order', function () {

    return view('sales_order');

})->middleware('auth');

//--------------------------------------------------------------------------INVOCIE --------------------------------------------

Route::get('/invoice','sales_order@invoiceData')->middleware('auth');


//--------------------------------------------------- REPORT ----------------------------------------------------

//Reports 

Route::get('/generateReport', 'sales_order@generateReport')->middleware('auth');

//---------------------------------------------------- OPENING AND CLOSING BALANCE ------------------------------------------------------

Route::get('/opening_closing_balance', function () {

    return view('opening_and_closing_balance');

})->middleware('auth');

//Opening and Closing Balance
Route::get('/generateBalance', 'sales_order@generateBalance')->middleware('auth');

//-------------------------------------------------------------------- SETTING PAGE -------------------------------------------------------

Route::get('/Setting','setting_controller@getSetttingData')->middleware('auth');
Route::get('/update_setting','setting_controller@updateSettingData')->middleware('auth');


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

//---------------------------------------------------------------------- FATS AND RATES -------------------------------------------------------

Route::get('/new_fats', function () {

    return view('add_new_fat_rates');

})->middleware('auth');

Route::post('/add_new_fats_record','fats_rate_controller@insertRecord')->middleware('auth');

Route::get('/all_fat_list','fats_rate_controller@allFatRates')->middleware('auth');

Route::post('/update_fats_record','fats_rate_controller@updateRecord')->middleware('auth');

Route::get('/delete_fat_rate','fats_rate_controller@disableRecord')->middleware('auth');

Route::post('/sales_all_fat_list','fats_rate_controller@get_all_fat_rates')->middleware('auth');


Route::get('/update_fat_rate', function () {

    return view('update_fat_rate');

})->middleware('auth');
