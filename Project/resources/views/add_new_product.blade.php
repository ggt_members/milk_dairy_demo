@extends('master')

 
	
@section('title')

		New Product
	
@endsection

@section('bredcum_title1')

		New Product
	
@endsection


@section('bredcum_title2')

		New Product
	
@endsection

@section('content')


			 <!-- Container fluid  -->
            <div class="container-fluid">
               
					<?php if( isset($_GET['status']) && $_GET['status']==1  ){  ?>	
							<div class="alert alert-success" style="color:grey">
                                       Record added successfully.
							</div>
                    <?php } ?>               
				
				
					<?php if( isset($_GET['status']) && $_GET['status']==0  ){  ?>	
								<div class="alert alert-danger" style="color:grey">
																   Failed to add new record.
								</div>
								  
					 <?php } ?>      
			   
			   
				
					<div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                
								

                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form action="add_new_product" method="post">
									
										<input type="hidden" name="_token" value="{{ csrf_token() }}">

										<div class="form-group">
                                            <label>Product Name</label>
                                            <input type="text" class="form-control" placeholder="Product Name" name="product_name" required>
                                        </div>
									
									<!--
                                        <div class="form-group">
                                            <label>Product Price </label>
                                            <input type="number" step="any" class="form-control" name="product_price" placeholder="Product Price" required>
                                        </div>									
									-->	
									
										<div class="form-group">
                                            <label>Product Discription</label>
                                            <input type="text" class="form-control" name="product_description" placeholder="Product Discription" >
                                        </div>
										
 										
                                        <button type="submit" class="btn btn-info" name="submit">Submit</button>
										
										
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				
				
				
				</div>
				
				
				
				
				
				
				
				
				
           </div>
        <!-- End Page wrapper  -->
		   <!-- End Container fluid  -->

	
@endsection