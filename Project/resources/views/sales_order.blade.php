@extends('master')

 
	
@section('title')

		Sales Order
	
@endsection

@section('bredcum_title1')

		Sales Order
	
@endsection


@section('bredcum_title2')

		Sales Order
	
@endsection

@section('content')

        <?php clearstatcache(); ?>
				
 <!-- Container fluid  -->
            <div class="container-fluid">
               
				
					<div class="row">
                    <div class="col-lg-12" >
                        <div class="card">
                            <div class="card-title">
                                
								

                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                   
									
									<form onsubmit="return display_alert()" >	
									
										<input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf_token">

										<div class="row col-md-12">
										
												<div class="form-group col-md-3" >
													<label>Customer Name</label>
													<select class="form-control" id="customer_name"  required style="height:43px" onchange="getSelectedCustomerDetails()"> 
													
														<option> Cust 1</option>
														<option> Cust 2</option>
														<option> Cust 3</option>
														<option> Cust 4</option>
														
													
													</select>
												</div>
												
												<div class="form-group col-md-3">
													<label>Contact No.</label>
													<input type="text" class="form-control" maxlength="10" id="customer_contact" placeholder="Contact No." required>
												</div>
										
												
											<!--	<div class="form-group col-md-2">
													<label>Invoice No.</label>
													<input type="text" class="form-control" placeholder="Invoice No." required>
												</div>
											-->	
												
												
												<div class="form-group col-md-3" >
													<label>Time</label>
													<select class="form-control" id="order_time"  required style="height:43px"  > 
													
														<option value="morning"> Morning</option>
														<option value="evening"> Evening</option>
														
													
													</select>
												</div>
												
												<div class="form-group col-md-3">
													<label>Date</label>
													<input type="date" class="form-control" id="order_date" placeholder="Date" value="<?php echo date('Y-m-d'); ?>" required>
												</div>
												
										</div>
									
										<hr size="10">
									
										<div class="row col-md-12">
										
												<div class="form-group col-md-3" >
													<label>Type</label>
													<select class="form-control" id="products_list"  required style="height:43px" > 
													
														<option> Cow Milk</option>
														<option> Buffalo Milk</option>
														<option> Goat Milk</option>
														<option> Sheep Milk</option>
														
													
													</select>
												</div>
												
											
												
												<div class="form-group col-md-2">
													<label>Total ML</label>
													<input type="number" step="any" class="form-control" placeholder="ML" id="liters" required onkeyup="calculate_total_price_of_single_order()">
												</div>
												
												
												
												<div class="form-group col-md-2" >
													<label>Fats</label>
													<select class="form-control" id="fats"  required style="height:43px" onchange="updateFatValue()"> 
													
														<option> 3.5</option>
														
													
													</select>
												</div>
												
												
												
												<div class="form-group col-md-2">
													<label>Price (per 100 ml)</label>
													<input type="number" step="any" class="form-control"  placeholder="Price" id="unit_price" required readonly>
												</div>
												
												<div class="form-group col-md-2">
													<label>Total.</label>
													<input type="number" step="any" class="form-control" placeholder="Total"  id="total" readonly required>
												</div>
										
												<div class="form-group col-md-1">
													<label><br></label>
													<input type="button" value="+" class="form-control btn btn-info" onclick="add_sale_order_row()">
												</div>
										</div> <!-- End of row -->
										
									
										<div class="row col-md-12" id="sales_order_div" style="display:none;margin-top:40px">
										
										
											<div class="col-lg-12">
													<div class="table-responsive">
														<table class="table table-bordered " id="sales_order_table">
															<thead>
																<tr>
																	
																	<th>Milk Type</th>
																	<th>ML</th>
																	<th>Fats</th>
																	<th>Price</th>
																	<th>Total</th>
																	<th style='text-align:center'>Remove</th>
																</tr>
															</thead>
															<tbody id="sale_order_rows">
																
															</tbody>
														</table>
													</div>
												</div>
												
												
												<div class=" row col-md-12" style="margin-top:40px">
												
														
														<div class="form-group col-md-3">
															<label>Total Amount</label>
															<input type="number" step="any" class="form-control" id="final_total_amt" placeholder="Total Amount"  required readonly>
														</div>
													
													
													
														<div class="form-group col-md-3">
															<label>Round Off</label>
															<input type="number" step="any" class="form-control" id="roundoff_amt" placeholder="Round Off"  required onkeyup="calculate_pending_amount()">
														</div>
													
														<div class="form-group col-md-3">
															<label>Paid Amount.</label>
															<input type="number" step="any" class="form-control" placeholder="Paid Amount"  id="paid_amount" required onkeyup="calculate_pending_amount()">
															<p style="color:red;padding-top:6px;display:none" id="amt_error">Amound should be less than round of amount.</p>
														</div>
										
														<div class="form-group col-md-3">
															<label>Pending Amount.</label>
															<input type="number" step="any" class="form-control"   id="pending_amount" required readonly>
														</div>
													
												
												</div>
												
												
												<div style="margin-top:40px" align="center">
												
													   <button align="center" type="submit" id="submit" class="btn btn-info"  >Submit</button> 
													 <!--  <button  align="center" type="button" id="submit" class="btn btn-info confirm-sales-order" >Submit</button> -->
												</div>
                       
                    <!-- /# column -->
										
											
										
										
										</div> <!-- End of row -->
									
									
											
												
									
									</form>	
										
                                       
                                   
                                </div>
                            </div>
                        </div>
                    </div>
				
				
				
				</div>
				
				
				
				
				
				
				
				
				
           </div>
        <!-- End Page wrapper  -->
		   <!-- End Container fluid  -->

<script>
window.onload = function() {
    getCustomerNames();
	getProductNames(1);
	getFatsList();
};
</script>
   
	
@endsection