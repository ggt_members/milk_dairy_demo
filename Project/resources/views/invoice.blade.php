@extends('master')

 
	
@section('title')

		Invoice
	
@endsection

@section('bredcum_title1')

		Invoice
	
@endsection


@section('bredcum_title2')

		Invoice
	
@endsection

@section('content')

 <link href="css/invoice.css" rel="stylesheet">
 				
 <!-- Container fluid  -->
            <div class="container-fluid">

  
	

  <div id="invoice-POS" >
    
    <center id="top" >
      
      <div class="info">  
       {{ $company_details[0]->company_name  }} <!--<br> /*{{ $company_details[0]->company_contact  }} */ -->
		
      </div><!--End Info-->
    </center><!--End InvoiceTop-->
    
    <div id="mid" style="margin-left:15px;margin-top:0px;padding-top:10px;">
      <div class="info">
        
        <p> Customer Name :{{ $cust_details[0]->customer_name  }}</br>                                  
			Date   : {{ $cust_details[0]->order_date  }} , {{ $cust_details[0]->order_time  }}</br>
			
			Receipt No. : {{ $cust_details[0]->order_id  }}
        </p>
      </div>
    </div><!--End Invoice Mid-->
    
    <div id="bot" style="margin-left:15px;color:black" >

					<div id="table"  >
						<table>
							<tr class="tabletitle" >
								<td class="item">Item</td>
								<td class="Hours">ML</td>
								<td class="Hours">Fats</td>
								<td class="Hours">Price</td>
								<td class="Rate">Total</td>
							</tr>

								@foreach($order_details as $record)
						
											<tr class="service">
												<td class="tableitem"><p class="itemtext">{{ $record->product_name }}</p></td>
												<td class="tableitem"><p class="itemtext">{{ $record->total_liters }}</p></td>
												<td class="tableitem"><p class="itemtext">{{ $record->total_fats }}</p></td>
												<td class="tableitem"><p class="itemtext">{{ $record->unit_price }}</p></td>
												<td class="tableitem"><p class="itemtext">{{ $record->total_price }}</p></td>
											</tr>
								@endforeach 
	
							<tr class="tabletitle" >
								<td></td> <td></td><td></td>
								<td class="Rate"><br>Total<br>Balance</td>
								<td class="payment"><br>{{ $cust_details[0]->round_off_amount  }}<br>{{ (double)$cust_details[0]->round_off_amount-(double)$cust_details[0]->paid_amount  }} </td>
							</tr>

							
						</table>
					</div><!--End Table-->

					<div id="legalcopy" align="center" style="display:none">
						<p class="legal"><strong>Thank you for your business!</strong>   
						</p>
					</div>

				</div><!--End InvoiceBot-->
  </div><!--End Invoice-->

	<div align="center" style="margin-top:20px">
			<button class="btn btn-info" onclick="PrintInvoice()" >Print</button>
	</div>
	
  <script>
  
	function PrintInvoice()
{
    var mywindow = window.open('', 'PRINT', 'height=400,width=600');

    mywindow.document.write('</head><body align="center">');
    mywindow.document.write(document.getElementById("invoice-POS").innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}

</script>
 
</div>
 
  @endsection