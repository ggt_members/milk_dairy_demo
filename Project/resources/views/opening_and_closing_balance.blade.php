@extends('master')

 
	
@section('title')

		Balance
	
@endsection

@section('bredcum_title1')

		Opening & Closing Balance
	
@endsection


@section('bredcum_title2')

		Opening & Closing Balance
	
@endsection

@section('content')


		    <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
              <!--  <div class="row">
			
					 <div class="col-md-3">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-shopping-cart f-s-40 color-success"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>1178</h2>
                                    <p class="m-b-0">Sales In Ltr.</p>
                                </div>
                            </div>
                        </div>
                    </div>
				

				   <div class="col-md-3">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-inr f-s-40 color-primary"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>22560</h2>
                                    <p class="m-b-0">Total Revenue In INR</p>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-md-3">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-inr f-s-40 color-success"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>21045</h2>
                                    <p class="m-b-0">Amount Paid By Cust.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-inr f-s-40 color-danger"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>1515</h2>
                                    <p class="m-b-0">Remaining Amount.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				-->
				
				<!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                
									<form action="generateBalance">
									<input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf_token">

										<div class="row col-md-12">
										
											
										
										   <div class="form-group col-md-3">
												<label>From Date</label>
												<input type="date" class="form-control" name="from_date"  value="<?php 
												
												if(!isset($_GET['from_date'])){
												echo date('Y-m-d'); }else{ echo $_GET['from_date']; } ?>"  >
											</div>
																			
											
										  
											
											
										 <div class="form-group col-md-3" style="padding-top:30px">
													<button type="submit" class="btn btn-info">Submit</button> 
										
											
											
											
										</div>	
                                    </form>
								
								
							
								
								
								
                               
							   </div>
                        </div>
                    </div>
				</div>	
				
				 <div class="row col-md-12">
			
					
				

				   <div class="col-md-4">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-inr f-s-40 color-primary"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>{{{ $opening }}}</h2>
                                    <p class="m-b-0">Opening Balance</p>
                                </div>
                            </div>
                        </div>
                    </div>
					
					<div class="col-md-4">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-inr f-s-40 color-warning"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>{{{number_format((double)$closing-(double)$opening, 2, '.', '')  }}}</h2>
                                    <p class="m-b-0">Earning of the day. </p>
                                </div>
                            </div>
                        </div>
                    </div>
					
					
                   
                    <div class="col-md-4">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-inr f-s-40 color-success"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>{{{ $closing }}}</h2>
                                    <p class="m-b-0">Closing Balance</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        
                    </div>
                </div>
				
				
				
				
	
                
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
	
	<script>
window.onload = function() {
    
	//getProductNames(2);
};
</script>
	
	
@endsection