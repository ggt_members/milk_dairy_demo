@extends('master')

 
	
@section('title')

		Update Customer Details
	
@endsection

@section('bredcum_title1')

		Update Customer Details
	
@endsection


@section('bredcum_title2')

		Update Customer Details
	
@endsection

@section('content')

		           <!-- Container fluid  -->
            <div class="container-fluid">

            <?php if( isset($_GET['status']) && $_GET['status']==1  ){  ?>  
                    <div class="alert alert-success" style="color:grey">
                               Record updated successfully .
                    </div>
            <?php } ?>               
            
            
            <?php if( isset($_GET['status']) && $_GET['status']==0  ){  ?>  
                    <div class="alert alert-danger" style="color:grey">
                                                       Failed to update record.
                    </div> 
            <?php } ?> 

            <?php if( isset($_GET['status']) && $_GET['status']==2  ){  ?>  
                    <div class="alert alert-success" style="color:grey">
                               Profile picture size must be less than 2mb.
                    </div>
            <?php } ?>
			
			
            <?php if( isset($_GET['delete_status']) && $_GET['delete_status']==1  ){  ?>  
                    <div class="alert alert-success" style="color:grey">
                               Record successfully deleted.
                    </div>
            <?php } ?>               
            
			  
            <?php if( isset($_GET['delete_status']) && $_GET['delete_status']==0  ){  ?>  
                    <div class="alert alert-danger" style="color:grey">
                                                       Failed to delete record.
                    </div> 
            <?php } ?> 
			
			
               
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="card">
                            <div class="card-body">
                             <!--   <h4 class="card-title">Data Table</h4>
                                <h6 class="card-subtitle">Data table example</h6> -->
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
												<th>#</th>
                                                <th>Name</th>
												 <th>Id</th>
                                                <th>Contact No</th>
                                                <th>Email Id</th>
                                                <th>Address</th>
                                                <th style="text-align:center;">Update</th>
                                               <th style="text-align:center;">Delete</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>      
                                            @foreach ($customerData as $retrive)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td> 
                                                    {{{ $retrive->customer_name }}} 
                                                    </td>
													<td> 
                                                    {{{ $retrive->customer_id }}} 
                                                    </td>
                                                    <td> 
                                                    {{{ $retrive->customer_contact }}} 
                                                    </td>
                                                    <td> 
                                                    {{{ $retrive->customer_email }}} 
                                                    </td>
                                                    <td> 
                                                    {{{ $retrive->customer_address }}} 
                                                    </td>
                                                    <td style="text-align:center;">
                                                    <a href="update_customer?id={{{ $retrive->customer_id  }}}">Update</a>
                                                    </td>
													
													 <td style="text-align:center;">
																
																<a href="delete_customer?id={{{ $retrive->customer_id  }}}&delete=1" onclick="return confirm('Are you sure?')">Delete</a>
                                                    </td>
													
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
				
				
				
				
				
				
           </div>
        <!-- End Page wrapper  -->
		   <!-- End Container fluid  -->
		
		
@endsection