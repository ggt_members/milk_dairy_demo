@extends('master')

 
	
@section('title')

		Update Product Details
	
@endsection

@section('bredcum_title1')

		Update Product Details
	
@endsection


@section('bredcum_title2')

		Update Product Details
	
@endsection

@section('content')

		           <!-- Container fluid  -->
            <div class="container-fluid">
               
				
					<div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                
								

                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form action="update_product_record" method="post" id="new_customer_form">
									
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input type="hidden" name="product_id" value="{{ $Record->product_id }}">

										<div class="form-group">
                                            <label>Product Name</label>
                                            <input type="text" class="form-control" placeholder="Product Name" name="product_name" required value="{{ $Record->product_name }}">
                                        </div>
									<!--
                                        <div class="form-group">
                                            <label>Product Price </label>
                                            <input type="number" step="any" class="form-control" name="product_price" placeholder="Product Price" required value="{{ $Record->product_price }}">
                                        </div>									
									-->	
                                     	<div class="form-group">
                                            <label>Product Discription</label>
                                            <input type="text" class="form-control" name="product_description" placeholder="Product Discription"  value="{{ $Record->product_description }}">
                                       	</div>
                                        <button type="submit" class="btn btn-info" >Update</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				
				
				
				</div>
				
				
				
				
				
				
				
				
				
           </div>
        <!-- End Page wrapper  -->
		   <!-- End Container fluid  -->
		
		
@endsection