@extends('master')

 
	
@section('title')

		New Customer
	
@endsection

@section('bredcum_title1')

		New Customer
	
@endsection


@section('bredcum_title2')

		New Customer
	
@endsection

@section('content')


			

			 <!-- Container fluid  -->
            <div class="container-fluid">
               
						
					<?php if( isset($_GET['status']) && $_GET['status']==1  ){  ?>	
							<div class="alert alert-success" style="color:grey">
                                       Record added successfully .
							</div>
                    <?php } ?>               
				
				
					<?php if( isset($_GET['status']) && $_GET['status']==0  ){  ?>	
								<div class="alert alert-danger" style="color:grey">
																   Failed to add new record.
								</div>
								  
					 <?php } ?>      
				
				
				
					<div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                
							                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                   <form action="add_new_customer" method="post" enctype="multipart/form-data">
									
										                         
										<input type="hidden" name="_token" value="{{ csrf_token() }}">

										<div class="form-group">
                                            <label>Customer Name</label>
                                            <input type="text" class="form-control" name="customer_name" placeholder="Customer Name" required>
                                        </div>
									
                                        <div class="form-group">
                                            <label>Email Address</label>
                                            <input type="email" class="form-control" name="customer_email"  placeholder="Email Address">
                                        </div>										
										
                                        <div class="form-group">
                                            <label>Contact No.</label>
                                            <input type="text" class="form-control" name="customer_contact" placeholder="Contact No." minlength="10" maxlength="10" required>
                                        </div>
                                        
										
                                        <div class="form-group">
                                            <label>Profile Pic.</label>
                                            <input type="file" class="form-control" name="customer_profile_pic" optional="size 2mb">
                                        </div>
										
										<div class="form-group">
                                            <label>Address</label>
                                            <textarea class="form-control" rows="5"  placeholder="Address." name="customer_address" required></textarea>
                                        </div>
                                        
										
										
                                        <button type="submit" class="btn btn-info" name="submit">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				
				
				
				</div>
				
				
				
				
				
				
				
				
				
           </div>
        <!-- End Page wrapper  -->
		   <!-- End Container fluid  -->

	
@endsection