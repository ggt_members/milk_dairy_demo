@extends('master')

 
	
@section('title')

		Setting
	
@endsection

@section('bredcum_title1')

		Setting
	
@endsection


@section('bredcum_title2')

		Setting
		
@endsection

@section('content')



		            <!-- Container fluid  -->
            <div class="container-fluid">
               
					<?php if( isset($_GET['status']) && $_GET['status']==1  ){  ?>	
							<div class="alert alert-success" style="color:grey">
                                       Record updated successfully .
							</div>
                    <?php } ?>               
				
				
					<?php if( isset($_GET['status']) && $_GET['status']==0  ){  ?>	
								<div class="alert alert-danger" style="color:grey">
																   Failed to update  record.
								</div>
								  
					 <?php } ?>      

			   
					<div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                
								

                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form action="update_setting" method="get">
									
										<div class="form-group">
                                            <label>Company Name</label>
                                            <input type="text" class="form-control" value="{{ $data['company_name'] }}" name="company_name"  placeholder="Company Name" required>
                                        </div>
									
									
										<div class="form-group">
                                            <label>Company Contact</label>
                                            <input type="text" class="form-control" maxlength="10" value="{{ $data['company_contact'] }}" name="company_contact"  placeholder="Company Contact" required>
                                        </div>
										
										<div class="form-group">
                                            <label>Company Address</label>
                                            <input type="text" class="form-control" value="{{ $data['company_address'] }}" name="company_address" placeholder="Company Address" required>
                                        </div>
											
                                        <button type="submit" class="btn btn-info">Update</button> <br><br><br>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				
				
				
				</div>
				
				
				
				
				
				
				
				
				
           </div>
        <!-- End Page wrapper  -->
		   <!-- End Container fluid  -->

	
@endsection