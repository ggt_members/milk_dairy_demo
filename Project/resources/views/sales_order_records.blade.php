@extends('master')
	
@section('title')

		Sales Order
	
@endsection

@section('bredcum_title1')

		Sales Order
	
@endsection


@section('bredcum_title2')

		Sales Order
	
@endsection

@section('content')

		           <!-- Container fluid  -->
            <div class="container-fluid">

			<?php $i=1; ?>
               
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="card">
                            <div class="card-body">
                             <!--   <h4 class="card-title">Data Table</h4>
                                <h6 class="card-subtitle">Data table example</h6> -->
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
												<th>#</th>
                                                <th>Customer</th>
                                                <th>Contact No</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>Total</th>
                                                <th>Round Off</th>
                                                <th>Paid</th>
                                                <th>Pending</th>
                                                <th style="text-align:center;">Print</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>      
                                            @foreach ($data as $record)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td> 
                                                    {{{ $record->customer_name }}} 
                                                    </td>
                                                    <td> 
                                                    {{{ $record->customer_contact }}} 
                                                    </td>
                                                    <td> 
                                                    {{{ $record->order_date }}} 
                                                    </td>
                                                    <td> 
                                                    {{{ $record->order_time }}} 
                                                    </td>
													
													
													 <td> 
                                                    {{{ $record->total_amount }}} 
                                                    </td>
													
													 <td> 
                                                    {{{ $record->round_off_amount }}} 
                                                    </td>
													
													 <td> 
                                                    {{{ $record->paid_amount }}} 
                                                    </td>
													
													 <td> 
                                                    {{{ (double)$record->round_off_amount - (double)$record->paid_amount }}} 
                                                    </td>
													
                                                    <td style="text-align:center;">
                                                    <a href="invoice?order_id={{{ $record->order_id  }}}">Invoice</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
				
				
				
				
				
				
           </div>
        <!-- End Page wrapper  -->
		   <!-- End Container fluid  -->
		
		
@endsection