<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    
	<title>@yield('title')</title>
	
    <!-- Bootstrap Core CSS -->
    <link href="css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
   
    <link href="css/lib/toastr/toastr.min.css" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
   
     <link href="css/lib/sweetalert/sweetalert.css" rel="stylesheet">
   
<link href="css/lib/calendar2/semantic.ui.min.css" rel="stylesheet">
    <link href="css/lib/calendar2/pignose.calendar.min.css" rel="stylesheet">
    <link href="css/lib/owl.carousel.min.css" rel="stylesheet" />
    <link href="css/lib/owl.theme.default.min.css" rel="stylesheet" />
  
   <link href="css/helper.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

	
	

</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="/index">
                        <!-- Logo icon -->
                        <b><img src="images/logo.png" alt="homepage" class="dark-logo" /></b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span>Milk Dairy</span>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        
                    </ul>
                   
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
               <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                       
						<li class="nav-label">Dashboard</li>
                      					   
										   <li> <a  href="/Portfolio/milk_dairy_demo/index" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard </a> </li>                           
						</li>

						
					  <li class="nav-label">Customers</li>
						  
							<li> <a  href="new_customer" aria-expanded="false"><i class="fa fa-user-plus"></i><span class="hide-menu">New Customer </a> </li>                           
							<li> <a  href="update_customer_list" aria-expanded="false"><i class="fa fa-refresh"></i><span class="hide-menu">Update Customer </a> </li>                           
					  </li> 

						<li class="nav-label">Product</li>
						  
							<li> <a  href="new_product" aria-expanded="false"><i class="fa fa-user-plus"></i><span class="hide-menu">New Product </a> </li>                           
							<li> <a  href="update_product_list" aria-expanded="false"><i class="fa fa-refresh"></i><span class="hide-menu">Update Product </a> </li>                           
					  </li> 


					<li class="nav-label">Fats & Rates</li>
						  
							<li> <a  href="new_fats" aria-expanded="false"><i class="fa fa-user-plus"></i><span class="hide-menu">Add New </a> </li>                           
							<li> <a  href="all_fat_list" aria-expanded="false"><i class="fa fa-refresh"></i><span class="hide-menu">Update Fat Values </a> </li>                           
					  </li> 					  
							
					
					  <li class="nav-label">Sales Orders</li>
                      	
						<li> <a  href="sales_order" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Manage Orders </a> </li>                           
						<li> <a  href="sales_order_history" aria-expanded="false"><i class="fa fa-eye"></i><span class="hide-menu">Sales Orders History </a> </li>                           
						
					  </li>		
		
					 <li class="nav-label">Reports</li>
                      
						<li> <a  href="generateReport" aria-expanded="false"><i class="fa fa-eye"></i><span class="hide-menu">Report </a> </li>                           
                    
					</li>   

					 <li class="nav-label">Other Options</li>
                      	<li> <a  href="generateBalance" aria-expanded="false"><i class="fa fa-inr"></i><span class="hide-menu">Opeing & Closing Balance </a> </li>                           
                      	<li> <a  href="Setting" aria-expanded="false"><i class="fa fa-gear"></i><span class="hide-menu">Setting </a> </li>                           
                      	<li> <a  href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" aria-expanded="false"><i class="fa fa-power-off"></i><span class="hide-menu">Logout </a> </li>      

						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

						
                      </li> 
						
						
                       
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->
		
			<!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">@yield('bredcum_title1')</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">@yield('bredcum_title2')</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
               

	
				
						@yield('content')	
			


                
                
          
           
       
      
    </div>
    <!-- End Page wrapper  -->
    </div> <!-- footer -->
            <footer class="footer"> © 2018 All rights reserved.designed by <a href="http://www.gauravgalaxytech.com/">GauravGalaxyTech.</a></footer>
            <!-- End footer -->
    <!-- End Wrapper -->
    <!-- All Jquery -->
	
	
	 <script src="js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="js/lib/bootstrap/js/popper.min.js"></script>
    <script src="js/lib/bootstrap/js/bootstrap.min.js"></script>
	
	
	
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>

   
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
	
	
		<script src="js/lib/calendar-2/moment.latest.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/semantic.ui.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/prism.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/pignose.calendar.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/calendar-2/pignose.init.js"></script>

	
   
	<script src="js/lib/datatables/datatables.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="js/lib/datatables/datatables-init.js"></script>
	
	
    <script src="js/lib/sweetalert/sweetalert.min.js"></script>
    <!-- scripit init-->
    <script src="js/lib/sweetalert/sweetalert.init.js"></script>
	
	<script src="js/sales_order.js"></script>
	<script src="js/customer_data.js"></script>
	<script src="js/product_data.js"></script>
	<script src="js/fat_and_rates.js"></script>
	
	<script>
	
		// disable mousewheel on a input number field when in focus
		// (to prevent Cromium browsers change the value when scrolling)
		$('form').on('focus', 'input[type=number]', function (e) {
		  $(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault()
		  })
		})
		$('form').on('blur', 'input[type=number]', function (e) {
		  $(this).off('mousewheel.disableScroll')
		})

</script>
	
	
</body>

</html>