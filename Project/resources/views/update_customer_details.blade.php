@extends('master')

 
	
@section('title')

		Update Customer Details
	
@endsection

@section('bredcum_title1')

		Update Customer Details
	
@endsection


@section('bredcum_title2')

		Update Customer Details
	
@endsection

@section('content')

		             <!-- Container fluid  -->
            <div class="container-fluid">

            	
               
				
					<div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                
								

                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form action="update_customer_record" method="post" id="new_customer_form" enctype="multipart/form-data">
									
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" name="cust_id" value="{{ $Record->customer_id }}">
									
									<div class="row col-md-12">
									
										<div class="col-md-6">
										
												<div class="form-group">
													<label>Customer Name</label>
													<input type="text" class="form-control" placeholder="Customer Name" value="{{ $Record->customer_name }}" name="customer_name" required>
												</div>
									
												<div class="form-group">
													<label>Email Address</label>
													<input type="email" class="form-control" placeholder="Email Address" value="{{ $Record->customer_email }}" name="customer_email">
												</div>										
												
												<div class="form-group">
													<label>Contact No.</label>
													<input type="text" class="form-control" placeholder="Contact No." minlength="10" maxlength="10" required name="customer_contact_no" value="{{ $Record->customer_contact }}">
												</div>
										
										</div>
		
										<div class="col-md-6">
										
											<br>
											
											<img src="customer_profile_pic/{{ $Record->customer_profile_picture }}" style="width:150px;height:143px" class="img-thumbnail" alt="Cinque Terre">
											
											<br><br>
											<div class="form-group">
                                            <label>Profile Pic.</label>
                                            <input type="file" name="customer_profile_pic" class="form-control" >
											</div>
                                        
										</div>
		
									</div>	
										
                                        
										
										<div class="form-group" style="margin-left:17px">
                                            <label>Address</label>
                                            <textarea class="form-control" rows="5"  placeholder="Address." required name="customer_address">{{ $Record->customer_address }}</textarea>
                                        </div>
                                        
										
										
                                        <button type="submit" class="btn btn-info" style="margin-left:17px">Update</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				
				
				
				</div>
				
				
				
				
				
				
				
				
				
           </div>
        <!-- End Page wrapper  -->
		   <!-- End Container fluid  -->
		
		
@endsection