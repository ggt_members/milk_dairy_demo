@extends('master')

 
	
@section('title')

		Update Fat Rates
	
@endsection

@section('bredcum_title1')

		Update Fat Rates
	
@endsection


@section('bredcum_title2')

		Update Fat Rates
	
@endsection

@section('content')


		 <!-- Container fluid  -->
            <div class="container-fluid">
                <?php if( isset($_GET['status']) && $_GET['status']==1  ){  ?>  
                    <div class="alert alert-success" style="color:grey">
                               Record updated successfully .
                    </div>
                <?php } ?>               
            
            
                <?php if( isset($_GET['status']) && $_GET['status']==0  ){  ?>  
                        <div class="alert alert-danger" style="color:grey">
                                                           Failed to update record.
                        </div> 
                <?php } ?>
				
				
				 <?php if( isset($_GET['delete_status']) && $_GET['delete_status']==1  ){  ?>  
                    <div class="alert alert-success" style="color:grey">
                               Record successfully deleted .
                    </div>
                <?php } ?>               
            
            
                <?php if( isset($_GET['delete_status']) && $_GET['delete_status']==0  ){  ?>  
                        <div class="alert alert-danger" style="color:grey">
                                                           Failed to delete record.
                        </div> 
                <?php } ?>
				
				
				
				
					<div class="card">
                            <div class="card-body">
                             <!--   <h4 class="card-title">Data Table</h4>
                                <h6 class="card-subtitle">Data table example</h6> -->
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
												<th>#</th>
                                                <th>Fat Value</th>
                                                
                                                <th>Price (Per 100 ml)</th>                                                
                                                <th style="text-align:center;">Update</th>
                                                <th style="text-align:center;">Delete</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($fats_rate_data as $retrive)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    
													<td> 
                                                    {{{ $retrive->fats_value }}} 
                                                    </td>
													
                                            
													<td> 
                                                    {{{ $retrive->rates_per_100_ml }}} 
                                                 	</td>
                                                 
													<td style="text-align:center;">
                                                    <a href="update_fat_rate?id={{{ $retrive->id  }}}&value={{{ $retrive->fats_value  }}}&rate={{{ $retrive->rates_per_100_ml  }}}">Update</a>
                                                    </td>
													
													<td style="text-align:center;">
                                                    <a href="delete_fat_rate?id={{{ $retrive->id  }}}" onclick="return confirm('Are you sure?')">Delete</a>
                                                    </td>
													
													
                                                </tr>
                                            @endforeach
                                            <!-- <tr>
												<td>1</td>
                                                <td>Cow Milk</td>
                                                <td>25</td>
                                                <td>xyz</td>
                                                <td style="text-align:center;"><a href="Update_Product_Details?name=Cow Milk&price=25&description=xyz">Update</a></td>
                                               
                                            </tr>-->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
				
				
				
				
				
				
           </div>
        <!-- End Page wrapper  -->
		   <!-- End Container fluid  -->

	
@endsection