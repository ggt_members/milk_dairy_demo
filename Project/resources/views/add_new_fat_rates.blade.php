@extends('master')

 
	
@section('title')

		New Fat Record.
	
@endsection

@section('bredcum_title1')

		New Fat Record.
	
@endsection


@section('bredcum_title2')

		New Fat Record.
	
@endsection

@section('content')


			 <!-- Container fluid  -->
            <div class="container-fluid">
               
					<?php if( isset($_GET['status']) && $_GET['status']==1  ){  ?>	
							<div class="alert alert-success" style="color:grey">
                                       Record added successfully.
							</div>
                    <?php } ?>               
				
				
					<?php if( isset($_GET['status']) && $_GET['status']==0  ){  ?>	
								<div class="alert alert-danger" style="color:grey">
										Failed to add new record.
								</div>
								  
					 <?php } ?>      
			   
			   
				
					<div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                
								

                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form action="add_new_fats_record" method="post">
									
										<input type="hidden" name="_token" value="{{ csrf_token() }}">

										<div class="form-group">
                                            <label>Fat Value</label>
                                            <input type="number" step="any" class="form-control" placeholder="Fat Value" name="fat_value" required>
                                        </div>
									
									
                                        <div class="form-group">
                                            <label>Price ( Per 100 ML )</label>
                                            <input type="number" step="any" class="form-control" name="fat_price" placeholder="Price Per 100 ML" required>
                                        </div>									
									
									
								
									
										
 										
                                        <button type="submit" class="btn btn-info" name="submit">Submit</button>
										
										
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				
				
				
				</div>
				
				
				
				
				
				
				
				
				
           </div>
        <!-- End Page wrapper  -->
		   <!-- End Container fluid  -->

	
@endsection