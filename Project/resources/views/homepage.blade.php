@extends('master')


@section('title')

		Dashboard
	
@endsection

@section('bredcum_title1')

		Dashboard
	
@endsection


@section('bredcum_title2')

		Dashboard
	
@endsection

@section('content')

		    <link href="css/lib/calendar2/semantic.ui.min.css" rel="stylesheet">
    <link href="css/lib/calendar2/pignose.calendar.min.css" rel="stylesheet">
  
		 
		 <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
                <div class="row">
			
					 <div class="col-md-3">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-shopping-cart f-s-40 color-success"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>{{ $start_pg_content['liters']/1000  }}</h2>
                                    <p class="m-b-0">Sales In Ltr.</p>
                                </div>
                            </div>
                        </div>
                    </div>
				

				   <div class="col-md-3">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-inr f-s-40 color-primary"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>{{ $start_pg_content['revenue']  }}</h2>
                                    <p class="m-b-0">Total Revenue In INR</p>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-md-3">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-inr f-s-40 color-warning"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>{{ $start_pg_content['paid_amt']  }}</h2>
                                    <p class="m-b-0">Amount Paid By Cust.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-shopping-cart f-s-40 color-danger"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>{{ $start_pg_content['fats']  }}</h2>
                                    <p class="m-b-0">Total Fats</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

				<div class="row">
					
					<?php $i=1; ?>
					
					
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                <h4>Recent Orders </h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>Total</th>
												<th>Paid</th>
												<th>Pending</th>
												
                                            </tr>
                                        </thead>
                                        <tbody>

										@foreach($records as $record)
						
                                            <tr>
                                                <td>{{ $i++ }}</td>                                               
                                                <td>{{ $record->customer_name }}</td>                                           
                                                <td>{{ $record->order_date }}</td>                                           
                                                <td>{{ $record->order_time }}</td>                                           
                                                <td>{{ $record->round_off_amount }}</td>                                           
                                                <td>{{ $record->paid_amount }}</td>                                           
                                                <td>{{ (double)$record->round_off_amount-(double)$record->paid_amount }}</td>                                           
                                                
                                               
                                            </tr>
                                        
										@endforeach   
											
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				
				
				
				
				
                <div class="row bg-white m-l-0 m-r-0 box-shadow ">

                   

                    <!-- column
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body browser">
								<br><br><br>
                                <p class="f-w-600">Cow Milk <span class="pull-right">35%</span></p>
                                <div class="progress ">
                                    <div role="progressbar" style="width: 35%; height:8px;" class="progress-bar bg-danger wow animated progress-animated"> <span class="sr-only">60% Complete</span> </div>
                                </div>

                                <p class="m-t-30 f-w-600">Buffalo Milk<span class="pull-right">45%</span></p>
                                <div class="progress">
                                    <div role="progressbar" style="width: 45%; height:8px;" class="progress-bar bg-info wow animated progress-animated"> <span class="sr-only">60% Complete</span> </div>
                                </div>

                                <p class="m-t-30 f-w-600">Goat Milk<span class="pull-right">10%</span></p>
                                <div class="progress">
                                    <div role="progressbar" style="width: 10%; height:8px;" class="progress-bar bg-success wow animated progress-animated"> <span class="sr-only">60% Complete</span> </div>
                                </div>

                                <p class="m-t-30 f-w-600">Other<span class="pull-right">10%</span></p>
                                <div class="progress">
                                    <div role="progressbar" style="width: 10%; height:8px;" class="progress-bar bg-warning wow animated progress-animated"> <span class="sr-only">60% Complete</span> </div>
                                </div>

								
                            </div>
                        </div>
                    </div>
                    <!-- column -->
					
					 <!-- column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
									<div class="year-calendar"></div>
								</div>
                        </div>
                    </div>
                    <!-- column -->
					
					
                </div>
                


                
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
			
			

	
@endsection