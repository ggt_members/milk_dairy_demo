@extends('master')

 
	
@section('title')

		Report
	
@endsection

@section('bredcum_title1')

		Report
	
@endsection


@section('bredcum_title2')

		Report
	
@endsection

@section('content')

            <?php $i=1; ?>
		    <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
              <!--  <div class="row">
			
					 <div class="col-md-3">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-shopping-cart f-s-40 color-success"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>1178</h2>
                                    <p class="m-b-0">Sales In Ltr.</p>
                                </div>
                            </div>
                        </div>
                    </div>
				

				   <div class="col-md-3">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-inr f-s-40 color-primary"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>22560</h2>
                                    <p class="m-b-0">Total Revenue In INR</p>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-md-3">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-inr f-s-40 color-success"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>21045</h2>
                                    <p class="m-b-0">Amount Paid By Cust.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-inr f-s-40 color-danger"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>1515</h2>
                                    <p class="m-b-0">Remaining Amount.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				-->
				
				<!-- Start Page Content -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                
									<form action="generateReport">
									<input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf_token">

										<div class="row col-md-12">
										
											<!-- <div class="form-group col-md-3">
												<label>Milk Type</label>
												<select class="form-control" style="height:43px" id="products_list">
														<option>All</option>
														<option>Cow Milk</option>
														<option>Buffalo Milk</option>
														<option>Goat Milk</option>
												</select>
											</div> -->
										
										   <div class="form-group col-md-3">
												<label>From Date</label>
												<input type="date" class="form-control" name="from_date" required value="<?php if(isset($_GET['from_date'])){ echo $_GET['from_date'];  } ?>">
											</div>
																			
											
										    <div class="form-group col-md-3">
												<label>To Date</label>
												<input type="date" class="form-control" required value="<?php if(isset($_GET['to_date'])){ echo $_GET['to_date'];  } ?>"
                                                name="to_date"  >
											</div>
													
											
											
										 <div class="form-group col-md-6" style="padding-top:30px">
													<button type="submit" class="btn btn-info col-md-2">Submit</button> 
													<button type="button" style="margin-left:10px" onclick="window.location.href='generateReport'" class="btn btn-info col-md-2">Reset</button> 
										
										</div>	
										
										 
										
										
                                    </form>
								
								
							
								
								
								
                                <div class="table-responsive m-t-40">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>Type</th>
                                                <th>ML</th>
                                                <th>Fats</th>
                                                 <th>Price (per 100 ml)</th>
                                               <th>Total INR</th>
                                               
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th>Type</th>
                                                <th>ML</th>
                                                <th>Fats</th>
                                                 <th>Price (per 100 ml)</th>
                                               <th>Total INR</th>
                                               
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @foreach ($data as $record)
                                                <tr>
                                                    <td>{{$i++}}</td>

                                                    <td> 
                                                    {{{ $record->customer_name }}} 
                                                    </td>

                                                    <td> 
                                                    {{{ $record->order_date }}} 
                                                    </td>

                                                    <td> 
                                                    {{{ $record->order_time }}} 
                                                    </td>

                                                    <td> 
                                                    {{{ $record->product_name }}} 
                                                    </td>

                                                     <td> 
                                                    {{{ $record->total_liters }}} 
                                                    </td>
                                                    
                                                     <td> 
                                                    {{{ $record->total_fats }}} 
                                                    </td>
                                                    
                                                     <td> 
                                                    {{{ $record->unit_price }}} 
                                                    </td>

                                                     <td> 
                                                    {{{ $record->total_price }}} 
                                                    </td>

                                                </tr>
                                            @endforeach


                                           <!--  <tr>
                                                <td>1</td>
                                                <td>Tiger Nixon</td>
                                                <td>18/04/2018</td>
                                                <td>9.00 AM</td>
                                                <td>Cow</td>
                                                <td>3</td>
                                                <td>26</td>
                                                <td>20</td>
                                                <td>60</td>
                                            </tr>
											
											 <tr>
                                                 <td>1</td>
                                               <td>Vijay jain</td>
                                                <td>19/04/2018</td>
                                                <td>6.00 PM</td>
                                                <td>Buffalo</td>
                                                <td>3</td>
                                                <td>35</td>
                                                <td>25</td>
                                                <td>75</td>
                                            </tr>
											
											
											 <tr>
                                                 <td>1</td>
                                               <td>Satish Singh</td>
                                                <td>19/04/2018</td>
                                                <td>9.30 AM</td>
                                                <td>Goat</td>
                                                <td>3</td>
                                                <td>39</td>
                                                <td>23</td>
                                               <td>69</td>
                                            </tr>
											 <tr>
                                                <td>1</td>
                                                <td>Tiger Nixon</td>
                                                <td>15/04/2018</td>
                                                <td>9.00 AM</td>
                                                <td>Cow</td>
                                                <td>3</td>
                                                <td>20</td>
                                                <td>20</td>
                                                <td>60</td>
                                            </tr>
											
											 <tr>
                                                 <td>1</td>
                                               <td>Vijay jain</td>
                                                <td>15/04/2018</td>
                                                <td>6.00 PM</td>
                                                <td>Buffalo</td>
                                                <td>3</td>
                                                <td>45</td>
                                                <td>25</td>
                                                <td>75</td>
                                            </tr>
											
											
											 <tr>
                                                 <td>1</td>
                                               <td>Satish Singh</td>
                                                <td>15/04/2018</td>
                                                <td>9.30 AM</td>
                                                <td>Goat</td>
                                                <td>3</td>
                                                <td>9</td>
                                                <td>23</td>
                                                <td>69</td>
                                            </tr>
											 <tr>
                                                 <td>1</td>
                                               <td>Tiger Nixon</td>
                                                <td>16/04/2018</td>
                                                <td>9.00 AM</td>
                                                <td>Cow</td>
                                                <td>3</td>
                                                <td>30</td>
                                                <td>20</td>
                                                <td>60</td>
                                            </tr>
											
											 <tr>
                                                 <td>1</td>
                                               <td>Vijay jain</td>
                                                <td>16/04/2018</td>
                                                <td>6.00 PM</td>
                                                <td>Buffalo</td>
                                                <td>3</td>
                                                <td>5</td>
												<td>25</td>
                                                <td>75</td>
                                            </tr>
											
											
											 <tr>
                                                 <td>1</td>
                                               <td>Satish Singh</td>
                                                <td>16/04/2018</td>
                                                <td>9.30 AM</td>
                                                <td>Goat</td>
                                                <td>3</td>
                                                <td>6</td>
                                                <td>23</td>
                                                <td>69</td>
                                            </tr>
											 <tr>
                                                 <td>1</td>
                                               <td>Tiger Nixon</td>
                                                <td>17/04/2018</td>
                                                <td>9.00 AM</td>
                                                <td>Cow</td>
                                                <td>3</td>
                                                <td>30</td>
                                                <td>20</td>
                                                <td>60</td>
                                            </tr>
											
											 <tr>
                                                <td>1</td>
                                                <td>Vijay jain</td>
                                                <td>17/04/2018</td>
                                                <td>6.00 PM</td>
                                                <td>Buffalo</td>
                                                <td>3</td>
                                                <td>75</td>
                                                <td>75</td>
                                                <td>75</td>
                                            </tr>
											
											
											 <tr>
                                                 <td>1</td>
                                               <td>Satish Singh</td>
                                                <td>17/04/2018</td>
                                                <td>9.30 AM</td>
                                                <td>Goat</td>
                                                <td>3</td>
                                                <td>9</td>
                                                <td>23</td>
                                                <td>69</td>
                                            </tr>
                             -->
                                     
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>	
				
				
				
				
				
	
                
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
	
	<script>
window.onload = function() {
    
//	getProductNames(2);
};
</script>
	
	
@endsection