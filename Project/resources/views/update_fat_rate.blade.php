@extends('master')

 
	
@section('title')

		Update Fat Record.
	
@endsection

@section('bredcum_title1')

		Update Fat Record.
	
@endsection


@section('bredcum_title2')

		Update Fat Record.
	
@endsection

@section('content')


			 <!-- Container fluid  -->
            <div class="container-fluid">
               
					
			   
				
					<div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-title">
                                
								

                            </div>
                            <div class="card-body">
                                <div class="basic-form">
                                    <form action="update_fats_record" method="post">
									
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">

										<div class="form-group">
                                            <label>Fat Value</label>
                                            <input type="number" step="any" class="form-control" value="<?php echo $_GET['value']; ?>" placeholder="Fat Value" name="fat_value" required>
                                        </div>
									
									
                                        <div class="form-group">
                                            <label>Price ( Per 100 ML )</label>
                                            <input type="number" step="any" class="form-control" value="<?php echo $_GET['rate']; ?>" name="fat_price" placeholder="Price Per 100 ML" required>
                                        </div>									
									
										
                                        <button type="submit" class="btn btn-info" name="submit">Update</button>
										
										
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				
				
				
				</div>
				
				
				
				
				
				
				
				
				
           </div>
        <!-- End Page wrapper  -->
		   <!-- End Container fluid  -->

	
@endsection