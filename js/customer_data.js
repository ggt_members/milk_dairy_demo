
	function getCustomerNames(){
				
			$("#customer_name").empty();
				
			var csrf=document.getElementById('csrf_token').value;
				
				
				$.ajax({
					url: 'all_customer_records',
					method: 'POST',
					data: {"_token": csrf },
					success: function(data){
				
						//alert(data);
						
						
						var myObj = $.parseJSON(data);
							
					
							$("#customer_name").empty();
							$("#customer_name").append("");
							 
							for (var i=0; i<myObj.length; i++) {
								 $('#customer_name').append('<option value="' + myObj[i]['customer_id'] + '">' + myObj[i]['customer_name'] + '</option>');
								 
							}
						
						 //	$('#customer_name').selectpicker('refresh');

							//TODO-update purchase party details
							getSelectedCustomerDetails();
						 
					}
				});
				
				
				
	}//End of function
	
	
	
//##################################################   GET SELECTED CUSTOMER DETAILS #########################################################################

		function getSelectedCustomerDetails(){

						
						
						var csrf=document.getElementById('csrf_token').value;
						var customer_id=document.getElementById('customer_name').value;

						//alert(customer_id);
						
							$.ajax({
								url: 'specific_customer_record',
								method: 'POST',
								data: {"_token": csrf , "id" : customer_id},
								success: function(data){

									//alert(data);
									
									var myObj = $.parseJSON(data);

									
									
										if (myObj['customer_contact']) {
									
											document.getElementById('customer_contact').value=myObj['customer_contact'].toString();
											//document.getElementById('customer_old_remain').value=myObj['customer_pending_amount'].toString();
											
											
											
											
										}
										//TODO-Update New Pending Amt
										//calculatePendingAmt();

								}
							});
								
				
			}//End of function
	
