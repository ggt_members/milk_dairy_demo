

//--------------------------------------------- ADD SALE ORDER RECORD --------------------------------------------

	var table_row_index=0;

	function add_sale_order_row(){

		var milk_type=document.getElementById("products_list").options[document.getElementById("products_list").selectedIndex].text;
		var milk_type_id=document.getElementById("products_list").value;
		var total_liters=document.getElementById("liters").value;
		var total_fats=document.getElementById("fats").options[document.getElementById("fats").selectedIndex].text;
		var milk_price=document.getElementById("unit_price").value;
		var total_price=document.getElementById("total").value;
		

		
		if(total_liters=="" || total_fats=="" || milk_price=="" || total_price==""){
			
			
			alert("Please enter values for liters.");
			
		}else{
		
			//Create new row 
			id="sales_order_table_row_"+table_row_index;		
			var new_row="<tr id='"+id+"'> <td id='"+$('#products_list').val()+"'>"+milk_type+"</td><td>"+total_liters+"</td><td>"+total_fats+"</td><td>"+milk_price+"</td><td>"+total_price+"</td><td style='text-align:center' onclick='delete_sale_order_row("+id+","+total_price+")'>Remove</td></tr>";
			
			//Append row to table
			document.getElementById("sale_order_rows").innerHTML+=new_row;
			
			table_row_index++;
			
			hide_and_display_sale_order_table(total_price,1);
		
		}

	}//End of add_sale_order function
	

	
//---------------------------------------- DELETE SALE ORDER ROW ------------------------------------------------------------
	
	function delete_sale_order_row(row_id,total_price){
		
		//alert(row_id);
		$(row_id).remove();
		
		hide_and_display_sale_order_table(total_price,0);
		
	}//End of delete_sale_order_row
	

//--------------------------------------------- HIDE AND DISPLAY SALES ORDER TABLE -------------------------------------------

	function hide_and_display_sale_order_table(total_price,flg){

				var rowCount = $('#sales_order_table tr').length;
				

				
				document.getElementById("paid_amount").value="";
				document.getElementById("pending_amount").value="";
					
	
				
				if(rowCount==1){
					
					document.getElementById('sales_order_div').style.display="none";
					document.getElementById("final_total_amt").value="";
					document.getElementById("roundoff_amt").value="";
					//document.getElementById("paid_amount").value="";
					//document.getElementById("pending_amount").value="";
	
				}else{
					
					document.getElementById('sales_order_div').style.display="block";
					
					
				
					
					
					var final_total_val=document.getElementById("final_total_amt").value;
					var roundoff_val=document.getElementById("roundoff_amt").value;
					
					if(final_total_val==""){
						final_total_val=0;
					}else{
						final_total_val=parseFloat(final_total_val);
					}
					
					if(roundoff_val==""){
						roundoff_val=0;
					}else{
						roundoff_val=parseFloat(roundoff_val);
					}
					
					
					if(flg==1){
						
						final_total_val+=parseFloat(total_price);
						document.getElementById("final_total_amt").value=final_total_val.toFixed(2);
						document.getElementById("roundoff_amt").value=final_total_val.toFixed(2);
						
					}else{
						
						
						final_total_val-=parseFloat(total_price);
						document.getElementById("final_total_amt").value=final_total_val.toFixed(2);
						document.getElementById("roundoff_amt").value=final_total_val.toFixed(2);
						
					}
					
					
				}
		
				
				

	} //End of function	


//--------------------------------------------- CALCULATE TOTAL PRICE ------------------------------------------------------------

	function calculate_total_price_of_single_order(){
			
			//alert("Hello");
			
			var total_liters=document.getElementById("liters").value;
			var milk_price=document.getElementById("unit_price").value;
	
			var total_amount=(parseFloat(total_liters)*parseFloat(milk_price)/100);
			
			document.getElementById("total").value=total_amount.toFixed(2);
	
	}//End of function	
	
	
//------------------------------------------------ CALCULATE FINAL TOTAL AMOUNT OF ORDER -----------------------------------------

		function calculate_pending_amount(){
			
					
				var total_roundoff_amount=document.getElementById("roundoff_amt").value;
				var total_paid_amount=document.getElementById("paid_amount").value;		
				
				
				
				
				var final_amount=check_val(total_roundoff_amount);
				var paid_amount=check_val(total_paid_amount);
				
				
				document.getElementById("pending_amount").value=(final_amount-paid_amount).toFixed(2);
				
				if(parseFloat(paid_amount)>parseFloat(final_amount)){
					
					//alert(final_amount+"=="+paid_amount);
				
					
					document.getElementById("amt_error").style.display="block";
					document.getElementById("submit").disabled=true;
					
				}else{
					
					
				
					document.getElementById("amt_error").style.display="none";
					document.getElementById("submit").disabled=false;
					
				}
				
				
				
				
				
			
		}//End of function
		
		
//----------------------------------------------------- CHECK EMPTY OR RETURN FOLAT VAL ------------------------------------------------

			function check_val(value){
				
				if(value==""){
						return 0;
				}else{
						return parseFloat(value).toFixed(2);
				}
				
			}//End of function
		
// ------------------------------------------------------------ SHOW SUBMIT CONFIRMATION DIALOGUE ----------------------------------------

function display_alert(){
    swal({
            title: "Do you really want to submit ?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "##26dad2",
            confirmButtonText: "Yes",
            closeOnConfirm: true
        },
        function(){
            //swal("Success !!", "Record added successfully", "success");
		   add_new_sale_entry();
        });
		
		return false;
};		
	
//------------------------------------------------------------- ADD NEW SALE ORDER ---------------------------------------------------------------


	function add_new_sale_entry(){
		
		var csrf=document.getElementById('csrf_token').value; 
		var rows = document.getElementById("sales_order_table").getElementsByTagName("tr").length;
				
		var jsonObj = [];		
		
		item={}
				 item ["customer_name"]=document.getElementById("customer_name").options[document.getElementById("customer_name").selectedIndex].text;
				 item ["customer_id"]=document.getElementById("customer_name").value;
				 item ["contact_no"]=document.getElementById("customer_contact").value;	
				 item ["time"]=document.getElementById("order_time").value;					     
				 item ["date"]=document.getElementById("order_date").value;	
				 item ["total_amount"]=document.getElementById("final_total_amt").value;	
				 item ["round_off"]=document.getElementById("roundoff_amt").value;	
				 item ["paid_amount"]=document.getElementById("paid_amount").value;	
				 
		jsonObj.push(item); 

			newObj= [];
			
				for(var i=1;i<parseInt(rows);i++){
					
					 item = {}
		
					 item ["product_id"] = document.getElementById("sales_order_table").rows[i].cells[0].getAttribute('id');
				     item ["liters"] = document.getElementById("sales_order_table").rows[i].cells[1].innerHTML;
				     item ["fats"]=document.getElementById("sales_order_table").rows[i].cells[2].innerHTML;
				     item ["unit_price"]=document.getElementById("sales_order_table").rows[i].cells[3].innerHTML;
				     item ["total_price"]=document.getElementById("sales_order_table").rows[i].cells[4].innerHTML;
					
					
					 newObj.push(item);
					
				}//End of for i

				jsonObj.push(newObj);
				var json= JSON.stringify(jsonObj);

				//alert(json);	
				
				
				
				
			$.ajax({
					url: 'insert_sales_order_entry',
					method: 'POST',	
					data: {"_token": csrf ,jsonData:json},				
					success: function(data){						
					//alert(data);

						if(data!=-1){

							//alert("Record Succefully added.");
							
							 window.location.href="invoice?order_id="+data;
							
								
						}else{

						
							sweetAlert("Oops...", "Something went wrong !!", "error");
							
							 //window.location.href="sales_order?status=0";
							 
							//document.getElementById("danger-alert").style.display = "block";
							//document.getElementById("danger-alert-msg").innerHTML="Failed to add new PO record.";
						} 

						//End of if case	
	
						
					}
				});
				
				
				
				
				
				
		
	}//End of sale entry function 

	
	
